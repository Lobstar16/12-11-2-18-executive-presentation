﻿using System;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Script.Serialization;
using Umbraco.Web.WebApi;
using System.IO;
using System.Net.Http;
using vadoc_public_website_redesign.Models;
using System.Xml.Linq;
using System.Xml;
using System.Xml.XPath;
using System.Web.Http;
using System.Net;

namespace vadoc_public_website_redesign.Controllers
{
    public class OffenderDataController : UmbracoApiController
    {
        [System.Web.Http.HttpPost]
        [System.Web.Mvc.AllowAnonymous]
        public HttpResponseMessage GetOffenders(OffenderSearchModel offenderQuery)
        {
            List<Offender> offenderResults = OffenderSearch(offenderQuery);
            if (offenderResults.Count < 1 || offenderResults == null)
            {

                string message = "No offender match was found using the information which was provided";
                HttpError err = new HttpError(message);
                return Request.CreateResponse(HttpStatusCode.NotFound, err);
            }
            else
            {
                return Request.CreateResponse(HttpStatusCode.OK, offenderResults);
            }
        }

        OffenderRepository OffenderRepo = new OffenderRepository();

        public List<Offender> OffenderSearch(OffenderSearchModel offender)
        {
            List<Offender> results = new List<Offender>();

            if (ModelState.IsValid && offender.OffenderId != null)
            {
                results = GetById(offender.OffenderId);
            }
            else if (!string.IsNullOrEmpty(offender.FirstNameLowercase) && !string.IsNullOrEmpty(offender.LastNameLowercase))
            {
                results = GetByFirstAndLastName(offender.FirstNameLowercase, offender.LastNameLowercase);
            }
            else
            {
                results = BadQuery();
            }

            var filteredResults = FilterByOptional(
                results,
                offender.MiddleNameLowercase,
                offender.ReleaseDate,
                offender.AgeFrom,
                offender.AgeTo,
                offender.Race,
                offender.Gender);

            return filteredResults;
        }


        public List<Offender> GetById(int? id)
        {
            return OffenderRepo.OffenderList.Where(item => item.OffenderId == id).ToList();
        }


        public List<Offender> GetByFirstAndLastName(string firstname, string lastname)
        {
            return OffenderRepo.OffenderList.Where(x => x.FirstNameLowercase.StartsWith(firstname) && x.LastNameLowercase == lastname).ToList();
        }


        private List<Offender> FilterByOptional(
            List<Offender> results,
            string middleName = null,
            string releaseDate = null,
            int? ageFrom = null,
            int? ageTo = 99,
            string race = null,
            string gender = null
        )
        {
            if (!string.IsNullOrEmpty(middleName))
            {
                results = results.Where(x => CalculateSimilarity(x.MiddleNameLowercase, middleName) > 0.6).ToList();
            }
            if (releaseDate != null && releaseDate != "")
            {
                results = results.Where(x => x.ProjectedReleaseDate == releaseDate).ToList();
            }
            if (ageFrom != null)
            {
                results = results.Where(x => x.Age >= ageFrom && x.Age <= ageTo).ToList();
            }
            if (!string.IsNullOrEmpty(race))
            {
                results = results.Where(x => x.Race.ToLower() == race).ToList();
            }
            if (!string.IsNullOrEmpty(gender))
            {
                results = results.Where(x => x.Gender.ToLower() == gender).ToList();
            }
            return results;
        }
        public List<Offender> BadQuery()
        {
            var offenders = new List<Offender>();
            return offenders;
        }

        public double CalculateSimilarity(string source, string target)
        {
            if ((source == null) || (target == null)) return 0.0;
            if ((source.Length == 0) || (target.Length == 0)) return 0.0;
            if (source == target) return 1.0;

            int stepsToSame = ComputeLevenshteinDistance(source, target);
            return (1.0 - ((double)stepsToSame / (double)Math.Max(source.Length, target.Length)));
        }
        public int ComputeLevenshteinDistance(string source, string target)
        {
            if ((source == null) || (target == null)) return 0;
            if ((source.Length == 0) || (target.Length == 0)) return 0;
            if (source == target) return source.Length;

            int sourceWordCount = source.Length;
            int targetWordCount = target.Length;

            // Step 1
            if (sourceWordCount == 0)
                return targetWordCount;

            if (targetWordCount == 0)
                return sourceWordCount;

            int[,] distance = new int[sourceWordCount + 1, targetWordCount + 1];

            // Step 2
            for (int i = 0; i <= sourceWordCount; distance[i, 0] = i++) ;
            for (int j = 0; j <= targetWordCount; distance[0, j] = j++) ;

            for (int i = 1; i <= sourceWordCount; i++)
            {
                for (int j = 1; j <= targetWordCount; j++)
                {
                    // Step 3
                    int cost = (target[j - 1] == source[i - 1]) ? 0 : 1;

                    // Step 4
                    distance[i, j] = Math.Min(Math.Min(distance[i - 1, j] + 1, distance[i, j - 1] + 1), distance[i - 1, j - 1] + cost);
                }
            }

            return distance[sourceWordCount, targetWordCount];
        }

    }
}