﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Linq;

namespace vadoc_public_website_redesign.Models
{
    public class JobRepository
    {
        private List<Job> allJobs;
        private XDocument JobsData;
        private List<int> costCodes = new List<int>() { 773, 749, 756, 735, 769, 785, 711, 772, 752, 701 };

        public JobRepository()
        {
            var server = HttpContext.Current.Server;
            try
            {
                allJobs = new List<Job>();
                JobsData =
                    XDocument.Load(server.MapPath("~/JobsData/jobsData.xml"));
                var Jobs = from x in JobsData.Descendants("Job")
                           select new Job(
                               x.Element("WorkingTitle").Value,
                               x.Element("Agency").Value,
                               x.Element("Location").Value,
                               x.Element("HiringRange").Value,
                               x.Element("JobCloseDate").Value,
                               x.Element("PostingID").Value
                           );
                allJobs.AddRange(Jobs.ToList<Job>());
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
            this.JobsList = allJobs;

        }
        public List<Job> JobsList { get; set; }
    }
}