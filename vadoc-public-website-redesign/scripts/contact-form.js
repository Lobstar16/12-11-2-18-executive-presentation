﻿// -------------------
// Globals
// -------------------
var formResults;
var contactForm;
var captchaWrap;
var captcha;
//------------------------------------------
// On completed contact form submission
//------------------------------------------
function submissionComplete() {
    $('html, body').animate({
        scrollTop: $(formResults).offset().top
    }, 200);
    initialize();
    clearForm();
}
// -----------------------------
// Clear Form
// -----------------------------
function clearForm() {
    contactForm.trigger("reset");
    autoFillCaptcha();
}
// -----------------------------
// Auto-fill Captcha field
// -----------------------------
function autoFillCaptcha() {
    captcha.val("river");
}
// -------------------
// Initialize Elements
// -------------------
function initialize() {
    formResults = $("#form-results");
    captchaWrap = $("#captcha-wrapper");
    captcha = $("#contact-robot");
    captchaWrap.hide();
    autoFillCaptcha();
}
// -------------------
// Document Ready
// -------------------
$(function () {
    contactForm = $("#contact-form");
    initialize();
});