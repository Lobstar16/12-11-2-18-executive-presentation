﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vadoc_public_website_redesign.Models
{
    public class Job
    {
        private string baseUrl = "https://virginiajobs.peopleadmin.com/postings/";
        public Job()
        {
            this.WorkingTitle = null;
            this.Agency = null;
            this.Location = null;
            this.HiringRangeMin = null;
            this.HiringRangeMax = null;
            this.JobCloseDate = null;
            this.Link = null;
        }

        public Job(string WorkingTitle, string Agency, string Location, string HiringRange, string JobCloseDate, string Id)
        {
            this.WorkingTitle = TruncateWorkingTitle(WorkingTitle);
            this.Agency = parseAgency(Agency);
            this.Location = parseLocation(Location);
            this.HiringRangeMin = parseSalaryMin(HiringRange);
            this.HiringRangeMax = parseSalaryMax(HiringRange);
            this.JobCloseDate = formatDate(JobCloseDate);
            this.Link = baseUrl + Id;
        }
        public string WorkingTitle { get; set; }
        public string Agency { get; set; }
        public string Location { get; set; }
        public string HiringRangeMin { get; set; }
        public string HiringRangeMax { get; set; }
        public string JobCloseDate { get; set; }
        public string Link { get; set; }

        protected static string TruncateWorkingTitle(string workingTitle)
        {
            return workingTitle.IndexOf(" - ", StringComparison.Ordinal) < 0
                       ? workingTitle
                       : workingTitle.Substring(0, workingTitle.IndexOf(" - ", StringComparison.Ordinal)).Trim();
        }
        protected static string parseSalaryMin(string salaryRange)
        {
            string parseOut = " - ";
            int idx = salaryRange.IndexOf(parseOut);

            return idx != -1
                ? salaryRange.Substring(1, idx).Trim()
                : salaryRange;
        }

        protected static string parseSalaryMax(string salaryRange)
        {
            string parseOut = " - ";
            int idx = salaryRange.IndexOf(parseOut);

            return idx != -1
                ? salaryRange.Substring(idx + parseOut.Length).Trim()
                : "";
        }
        protected static string formatDate(string closeDate)
        {
            return closeDate.Replace("-", "/");
        }
        protected static string parseLocation(string location)
        {
            if (string.IsNullOrEmpty(location))
                location = "Not Available";

            location = location.IndexOf(" -", StringComparison.Ordinal) < 0
                       ? location
                       : location.Substring(0, location.IndexOf(" -", StringComparison.Ordinal)).Trim();

            return location;
        }
        protected static string parseAgency(string agency)
        {
            if (string.IsNullOrEmpty(agency))
            {
                agency = "Not Available";
            }

            agency = agency.IndexOf(" (", StringComparison.Ordinal) < 0
                       ? agency
                       : agency.Substring(0, agency.IndexOf(" (", StringComparison.Ordinal)).Trim();

            return agency;
        }
    }
}