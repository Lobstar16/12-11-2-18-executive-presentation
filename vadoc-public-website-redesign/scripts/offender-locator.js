// -------------------
// Globals
// -------------------
var searchResults;
var resultsList;
var resultsSort;
var endpoint = '/Umbraco/Api/OffenderData/GetOffenders';
var sortButtons;
var resultCards;
var locatorForm;

// ------------------------------
// On offender search success
//-------------------------------
function searchSuccess() {
    searchResults = $("#offender-search-results");
    resultsList = $("#result-cards");
    resultsSort = $('#results-sort');
    sortButtons = resultsSort.find('button');
    resultCards = $(resultsList).find('.result')
    if (searchResults.length) {
        $('html, body').animate({
            scrollTop: $(searchResults).offset().top
        }, 200);
    }
    initialize();
    sortSetup();
    setupTooltips();
}
//--------------------------------------
// Show tooltips on hover
//--------------------------------------
function setupTooltips() {
    $('.card-icon').hover(function (event) {
        $(event.target).parent().parent().find('.tooltip').toggle();
    }, function () {
        $('.tooltip').hide();
    });
}
// ------------------------------------
// Set up sort button functionality
// ------------------------------------
function sortSetup() {
    $.each(sortButtons, function (index, button) {
        $(button).click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            sortCards(button.id);
        });
    });
}
// --------------------------
// Clear result cards
// --------------------------
function clearCards() {
	console.log('clear!!!')
	resultsList.empty();
}
// -----------------------------------------
// Populate sorted offender result cards
// -----------------------------------------
function populateSortedCards() {
	$.each(resultCards, function(i, card) {
		var listItem = card;
		resultsList.append(listItem);
	})
}
// ----------------------------
// Results sort functionality
// ----------------------------
function sortCards(buttonId) {
	var button = document.getElementById(buttonId);

	sortButtons.removeClass('active');
	$(button).addClass('active');

	resultCards.sort(function(a, b) {
		if(getNodeVal(a, buttonId) > getNodeVal(b, buttonId)) {
			return 1;
		} else {
			return -1;
		}
	});
	populateSortedCards();
}
function getNodeVal(card, parameter) {
	var sortVal;
	if(parameter === 'name-sort') {
		sortVal = $(card).find('.name').text();
	} else if(parameter === 'location-sort') {
		sortVal = $(card).find('.location').text();
	} else if(parameter === 'salary-sort') {
		sortVal = parseInt($(card).find('.job-sal-min').text());
	} else if(parameter === 'date-sort') {
		var dateText = $(card).find('.date').text()
		var date = new Date(Date.parse(dateText));
		sortVal = date;
	}
	return sortVal;
}
// Check for minimum required search input
function requiredInputs() {
	var firstName = $('#first-name').val();
	var lastName = $('#last-name').val();
	var id = $('#offender-id').val();
	return ((firstName && lastName) || id); 
}
function alertRequired() {
	var requirementsText = $('#form-requirements p').text();
	requirementsText = '*' + requirementsText;
	$('#form-requirements').addClass('form-error');
	$('#form-requirements p').addClass('input-err').text(requirementsText);
}
function removeRequirementsAlert() {
	var requirementsText = $('#form-requirements p').text();
	requirementsText = requirementsText.replace('*', '');
	$('#form-requirements').removeClass('form-error');
	$('#form-requirements p').removeClass('input-err').text(requirementsText);
}

//-------------------------------
// Initialize new form elements
//-------------------------------
function initialize() {
    locatorForm = $('#offender-locator-form');
    searchResults = $("#offender-search-results");
    resultsList = $("#result-cards");
    resultsSort = $('#results-sort');
    resultsSortTitle = resultsSort.find(".button-group-title")
    resultsSortButtonGroup = resultsSort.find("ul");
    sortButtons = resultsSort.find('button');
    resultCards = $(resultsList).find('.result')
    var errorDisplay = $('.no-offender-results');
    var errMessage = errorDisplay.find('#search-err-message');
    var searchErrDisclaimer = errorDisplay.find('#search-err-disclaimer');

    // If page load with search results
    if (searchResults.length) {
        resultsSortTitle.show();
        resultsSortButtonGroup.show();
        setupTooltips();
        sortSetup();
    }
}
// -------------------
// Document Ready
// -------------------
$(function () {
	initialize();
});