// Globals
var isErr = false;
// Simple Pattern Validations
var alphaRegex = /^[a-zA-Z]+$/;
var alphaUnstrictRegex = /^[a-zA-Z\s|\(|\)]*$/;
var alphaHyphenRegex = /^[a-zA-Z-]+$/
var alphanumericRegex = /^[0-9a-zA-Z]+$/;
var numericRegex = /^\d+$/;
var forbiddenCharsRegex = /\`|\~|\%|\*|\+|\=|\[|\{|\]|\}|\||\<|\>/;

function patternValidation(regex, input, errorMessage) {
	input = input.trim();
	if(input.match(regex)) {
		return false;
	}
	return errorMessage;
};

function isBlank(input) {
	return (input == undefined || input  == ''); 
};

function isAlpha(input) {
	var errorMessage = 'Alphabetic characters only';
	return patternValidation(alphaRegex, input, errorMessage);
};

function isAlphaUnstrict(input) {
	var errorMessage = 'Alphabetic characters only';
	return patternValidation(alphaUnstrictRegex, input, errorMessage);
};

function isAlphaHyphen(input) {
	var errorMessage = 'Alphabetic characters only';
	return patternValidation(alphaHyphenRegex, input, errorMessage);
};

function isAlphanumeric(input) {
	var errorMessage = 'Alphanumeric characters only';
	return patternValidation(alphanumericRegex, input, errorMessage);
};

function isNumeric(input) {
	var errorMessage = 'Numbers only';
	return patternValidation(numericRegex, input, errorMessage);
};

function hasForbiddenChars(input) {
	var errorMessage = 'No special characters';
	input = input.trim();
	if(!(input.match(forbiddenCharsRegex))) {
		return ;
	}
	return errorMessage;
};

//  Complex Pattern Validations
var completePhoneRegex = /^\(?\d{3}\)?[- ]?\d{3}[- ]?\d{4}$/;
var incompletePhoneRegex = /^[0-9-]*$/;
var emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
var zipcodeRegex = /(^\d{5}$)|(^\d{5}-\d{4}$)/;
var nameRegex = /^[a-zA-Z \-\']+(?:\s[a-zA-Z]+)*$/;
var dateRegex = /(\d+)(-|\/)(\d+)(?:-|\/)(?:(\d+)\s+(\d+):(\d+)(?::(\d+))?(?:\.(\d+))?)?/;

function isValidName(input) {
	var errorMessage = 'Must be a valid name.';
	return patternValidation(nameRegex, input, errorMessage);
};

function isCompletePhone(input) {
	var errorMessage = 'Must be a valid phone number';
	return patternValidation(completePhoneRegex, input, errorMessage);
};

function isIncompletePhone(input) {
	var errorMessage = 'Numeric characters only';
	return patternValidation(incompletePhoneRegex, input, errorMessage);
};

function isValidEmail(input) {
	var errorMessage = 'Must be a valid email address';
	return patternValidation(emailRegex, input, errorMessage);
};

function isValidZipcode(input) {
	var errorMessage = 'Must be a valid zipcode.';
	return patternValidation(zipcodeRegex, input, errorMessage);
};

function isValidDate(input) {
	var errorMessage = 'Must follow date format dd/mm/yyyy';
	return patternValidation(dateRegex, input, errorMessage);
}

function isLastFourSsn(input) {
	var errorMessage = 'Must be a four digit number';
	if(!isNumeric(input) && input.length === 4) {
		return false;
	} else {
		return errorMessage;
	}
}

//Field Masks
function fieldMask(event, maskPoints, maskChar) {
	var val = event.target.value;
	for(var i = 0; i < maskPoints.length; i++) {
	    if (val.length == maskPoints[i] && event.key != maskChar) {
	        event.target.value += maskChar;
	    } 
	}
    return;
};

// Class validation combinations
function getValidationCombo(attribute) {
	var combo;

	if(attribute === 'zipcode') {
		combo = [isValidZipcode];
	} else if(attribute === 'name') {
		combo = [isValidName];
	} else if(attribute === 'phone') {
		combo = [isIncompletePhone, isCompletePhone];
	} else if(attribute === 'alpha-unstrict') {
		combo = [isAlphaUnstrict];
	} else if(attribute === 'email') {
		combo = [isValidEmail];
	} else if(attribute === 'unstrict') {
		combo = [hasForbiddenChars];
	} else if(attribute === 'numeric') {
		combo = [isNumeric];
	} else if(attribute === 'alpha-numeric') {
		combo = [isAlphanumeric];
	} else if(attribute === 'date') {
		combo = [isValidDate];
	} else if(attribute === 'last-four-ssn') {
		combo = [isLastFourSsn];
	}

	return combo;
}
// Display error messages 
function displayTextErrors(input, errors) {
	var errorDisplay = $(input).siblings('.form-error');
	errorDisplay.empty();
	if(errors.length > 0) {
		$(input).addClass('input-err');
		for(var i = 0; i < errors.length; i++) {
			errorDisplay.append(
					'<p>*' + errors[i] + '</p>'
				);
		}
		$(input).on('change', function() {
			validateInput(input);
		});
		$(input).on('keyup', function() {
			validateInput(input);
		});
	} else {
		$(input).removeClass('input-err');
		$(input).off('change');
		$(input).off('keyup');
	}
};

function displayMultiSelectError(fieldset, error) {
	var errorDisplay = $(fieldset).find('.form-error');
	errorDisplay.empty();
	
	if(error) {
		errorDisplay.append(
					'<p>*' + error + '</p>'
				);
		$(fieldset).on('click', 'input', function() {
			validateMultiSelect(fieldset);
		});
	} else {
		$(fieldset).off('change', 'input');
	}
}
// Validate individual form field
function validateInput(input) {
	var inputVal = $(input).val();
	var validationCombo = getValidationCombo($(input).data('validate'));
	var errorMessages = [];
	var isErr = false;

	if($(input).attr('required') && (isBlank(inputVal))) {
		errorMessages.push('This field is required');
	} else if(!(isBlank(inputVal))) {
		for(var i = 0; i < validationCombo.length; i++) {
			var cb = validationCombo[i];
			if((cb(inputVal))) {
				errorMessages.push(cb(inputVal));
				isErr = true;
			}
		}
	}
	displayTextErrors(input, errorMessages);
	return isErr;
}
// Validate set of radio/checkboxes 
function validateMultiSelect(fieldset) {
	var required = $(fieldset).find('input[required]');
	var inputs = $(fieldset).find('input');
	var notSelected = "Please select one";
	var isErr = false;
	if(required.length > 0) {
		$(inputs).each(function(index, input) {
			if(input.checked) {
				notSelected = false;
			} 
		});
		displayMultiSelectError(fieldset, notSelected);
	}
	if(notSelected) {
		isErr = true;
	}
	return isErr;
}
// Scroll to first error
function scrollToErr() {
	console.log('err scroll')
	$('html, body').animate({
	    scrollTop: $('.input-err').offset().top
	}, 0);
}

// Validate all relevant form text fields
function validateAllText(inputs) {
	var isErr = false;
	inputs.each(function(index, input) {
		if(validateInput(input)) {
			isErr = true;
		}
	});
	if(isErr) {
		scrollToErr();
	}
	return isErr;
};

// Validate all relevant radio/checkboxes
function validateAllRadio(fielsets) {
	var isErr = false;
	fielsets.each(function(index, fieldset) {
		if(validateMultiSelect(fieldset)) {
			isErr = true;
		}
	});
	if(isErr) {
		scrollToErr();
	}
	return isErr;
};

// Document ready
$(function() {
	// var needsTextValidation = $('.validate');
	// Add auto-formatting to telephone inputs
	if($('input[type="tel"]')) {
		$('input[type="tel"]').on('keypress', function(e) {
			fieldMask(e, [3, 7], '-')
		});
	}
	// Add auto-formatting to date inputs
	if($('input[type="date"]')) {
		$('input[data-validate="date"]').on('keypress', function(e) {
			fieldMask(e, [2, 5], '/')
		});
	}
	// needsTextValidation.on('blur', function(event) {
	// 	validateInput(event.target);
	// });
})