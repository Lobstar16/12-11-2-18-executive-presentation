//-------------------------
// Job Openings Page
// ------------------------
var resultsSort;
var sortButtons;
var jobCards;
var jobCardList;
var featureCards;

// Job Card UI
function sortCards(buttonId) {
    var button = document.getElementById(buttonId);
    var order = $(button).attr('data-order');

    if ($(button).hasClass('active')) {
        $(button).attr('data-order', order * -1);
    } else {
        $(button).attr('data-order', 1);
    }
    order = $(button).attr('data-order');

    sortButtons.removeClass('active');
    $(button).addClass('active');

    jobCards.sort(function (a, b) {
        if (getNodeVal(a, buttonId) > getNodeVal(b, buttonId)) {
            return order;
        } else {
            return order * -1;
        }
    });
    clearCards();
    populateSortedCards();
}
function getNodeVal(card, parameter) {
    var sortVal;
    if (parameter === 'name-sort') {
        sortVal = $(card).find('.job-name').text();
    } else if (parameter === 'location-sort') {
        sortVal = $(card).find('.job-city-state').text();
    } else if (parameter === 'salary-sort') {
        sortVal = parseInt($(card).find('.job-sal-min').text());
    } else if (parameter === 'date-sort') {
        sortVal = new Date($(card).find('.job-close-date').text());
    }
    return sortVal;
}
function clearCards() {
    jobCardList.remove('li');
}
function populateSortedCards() {
    $.each(jobCards, function (i, card) {
        var listItem = card;
        jobCardList.append(listItem);
    })
}
// Initialize
function initializeCards() {
    //Set defaults
    jobCards = $('#result-cards li');
    //Show Job card sorting UI
    jobCardList = $('#result-cards');
    resultsSort = $('#results-sort');
    sortButtons = $('#results-sort button');
    featureCards = $('.jc-with-image');
    resultsSort.show();
    resultsSort.attr('aria-hidden', 'false');

    //Add sorting function to each result sort button
    $.each(sortButtons, function (index, button) {
        $(button).click(function (e) {
            e.stopPropagation();
            e.preventDefault();
            sortCards(button.id);
        });
    });
    // Add hover state to cards
    jobCards.hover(function () {
        $(this).css({
            'background-color': '#eef2f4',
            'cursor': 'pointer'
        });
    },
        function () {
            $(this).css({
                'background-color': '#fff'
            });
        });
    // Add Clickable link UI to cards
    jobCards.on('click', function () {
        jobLink = $(this).find('.job-link').find('a').attr('href');
        console.log(jobLink)
        window.location = jobLink;
    });

    // Default order of cards
    sortCards('name-sort');
}
// ----------------------------
$(function () {
    initializeCards();
});