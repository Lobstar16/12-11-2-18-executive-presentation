﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Xml.Linq;

namespace vadoc_public_website_redesign.Models
{
    public class OffenderRepository
    {
        private List<Offender> allOffenders;
        private XDocument OffenderData;

        public OffenderRepository()
        {
            var server = HttpContext.Current.Server;
            try
            {
                allOffenders = new List<Offender>();
                OffenderData =
                    XDocument.Load(server.MapPath("~/OffenderData/Offenders.xml"));
                var Offenders = from x in OffenderData.Descendants("Offender")
                                select new Offender(
                                    Int32.Parse(x.Element("OffenderId").Value),
                                    x.Element("FirstNameLowerCase").Value,
                                    x.Element("MiddleNameLowerCase").Value,
                                    x.Element("LastNameLowerCase").Value,
                                    x.Element("FullName").Value,
                                    Int32.Parse(x.Element("Age").Value),
                                    x.Element("Gender").Value,
                                    x.Element("Race").Value,
                                    x.Element("ProjectedReleaseDate").Value,
                                    x.Element("DOCResponsible").Value,
                                    x.Element("LocationName").Value
                                );
                allOffenders.AddRange(Offenders.ToList<Offender>());
            }
            catch (Exception)
            {
                throw new NotImplementedException();
            }
            this.OffenderList = allOffenders;

        }
        public List<Offender> OffenderList { get; set; }
    }
}