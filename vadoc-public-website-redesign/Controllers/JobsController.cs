﻿using System;
using System.Collections.Generic;
using vadoc_public_website_redesign.Models;
using System.Net;
using Umbraco.Web.WebApi;
using System.Net.Http;

namespace vadoc_public_website_redesign.Controllers
{
    public class JobsController : UmbracoApiController
    {
        JobRepository JobRepo = new JobRepository();
        // GET: Jobs
        public HttpResponseMessage GetJobs()
        {
            return Request.CreateResponse(HttpStatusCode.OK, JobRepo.JobsList);
        }
    }
}