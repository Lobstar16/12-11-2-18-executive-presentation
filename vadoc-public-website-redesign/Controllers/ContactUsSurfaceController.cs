﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vadoc_public_website_redesign.Models;
using vadoc_public_website_redesign.Crm;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Sdk.Messages;
using System.Net;
using System.ServiceModel.Description;

namespace vadoc_public_website_redesign.Controllers
{
    public class ContactUsSurfaceController : SurfaceController
    {
        private readonly string _crmServiceAccountUserName =
            ConfigurationManager.AppSettings["CrmServiceAccountUserName"];
        private readonly string _crmServiceAccountPassword =
            ConfigurationManager.AppSettings["CrmServiceAccountPassword"];
        private readonly string _crmServiceAccountUrl =
            ConfigurationManager.AppSettings["CrmServiceUrl"];
        private const string Status = "statuscode";

        private DocServiceContext ServiceContext;

        // View path for contact partials
        public string GetViewPath(string name)
        {
            return $"/Views/Partials/ContactUs/{name}.cshtml";
        }
        // GET: Contact Us Form
        public ActionResult RenderContactForm()
        {
            return PartialView(GetViewPath("ContactUsForm"), new ContactUsFormModel());
        }

        private bool FindBestContact(Contact proposedContact, out Contact contact)
        {
            if (proposedContact == null)
            {
                contact = new Contact();
                return false;
            }

            var proposedContactLastName = proposedContact.LastName;
            var proposedEmail = proposedContact.EMailAddress1;

            var contacts = ServiceContext.ContactSet.Where(x =>
                x.LastName != null &&
                x.LastName == proposedContactLastName &&
                x.EMailAddress1 != null &&
                x.EMailAddress1 == proposedEmail);

            contact = contacts.Where(x => x.StateCode.Value == ContactState.Active).OrderByDescending(x => x.CreatedOn)
                .FirstOrDefault();

            if (contact != default(Contact)) return true;

            contact = contacts.Where(x => x.StateCode.Value == ContactState.Inactive)
                .OrderByDescending(x => x.CreatedOn).FirstOrDefault();

            if (contact != default(Contact)) return true;

            contact = proposedContact;

            return false;
        }

        // Submit Form
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult SubmitContactForm(ContactUsFormModel model)
        {
            if(!TempData.ContainsKey("CustomErrors"))
            {
                TempData.Add("CustomErrors", new List<string>());
            } 
            
            if (!ModelState.IsValid)
            {
                ((List<string>)TempData["CustomErrors"]).Add("Please Fix the following form errors.");
                TempData["JsDisabled"] = true;
                TempData["FormSuccess"] = false;
                TempData["InvalidModel"] = true;

                return CurrentUmbracoPage();
            }

            string message;
            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = _crmServiceAccountUserName;
                clientCredentials.UserName.Password = _crmServiceAccountPassword;

                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var organizationServiceProxy =
                new OrganizationServiceProxy(new Uri(_crmServiceAccountUrl), null,
                    clientCredentials, null);
                organizationServiceProxy.EnableProxyTypes();

                ServiceContext = new DocServiceContext(organizationServiceProxy);

                if (organizationServiceProxy != null)
                {
                    Guid userid = ((WhoAmIResponse)organizationServiceProxy.Execute(new WhoAmIRequest())).UserId;

                    if (userid != Guid.Empty)
                    {
                        message = "Connection Established Successfully...";
                        TempData["Message"] = message;
                        TempData["Guid"] = userid;
                    }
                }
                else
                {
                    ((List<string>)TempData["CustomErrors"]).Add("Failed to establish connection");
                }
            }
            catch (Exception ex)
            {
                ((List<string>)TempData["CustomErrors"]).Add("Failed to establish connection");
            }

            var contact = new Contact { LastName = model.LastName.Trim(), EMailAddress1 = model.Email.Trim() };
            var foundContact = FindBestContact(contact, out contact);

            if (foundContact)
            {
                TempData["FoundContact"] = true;
                ServiceContext.UpdateObject(contact);
            }
            else
            {
                TempData["FoundContact"] = true;
                ServiceContext.AddObject(contact);
            }

            contact.FirstName = model.FirstName;

            var telephone = model.Telephone ?? string.Empty;
            telephone = new string(telephone.Where(char.IsDigit).ToArray());
            if (!string.IsNullOrEmpty(telephone))
            {
                contact.Telephone1 =
                    $"({telephone.Substring(0, 3)}) {telephone.Substring(3, 3)}-{telephone.Substring(6, 4)}";
                contact.crspnd_BusinessExtension = telephone.Length > 10 ? telephone.Substring(10) : null;
            }

            if (!string.IsNullOrEmpty(model.AddressLine1) || !string.IsNullOrEmpty(model.AddressLine2) ||
                !string.IsNullOrEmpty(model.AddressCity) || !string.IsNullOrEmpty(model.PostalCode))
            {
                contact.Address1_Line1 = null;
                contact.Address1_Line2 = null;
                contact.Address1_City = null;
                contact.Address1_StateOrProvince = null;
                contact.Address1_PostalCode = null;

                if (!string.IsNullOrEmpty(model.AddressLine1))
                    contact.Address1_Line1 = model.AddressLine1;

                if (!string.IsNullOrEmpty(model.AddressLine2))
                    contact.Address1_Line2 = model.AddressLine2;

                if (!string.IsNullOrEmpty(model.AddressCity))
                    contact.Address1_City = model.AddressCity;

                contact.Address1_StateOrProvince = model.AddressState;

                if (!string.IsNullOrEmpty(model.PostalCode))
                    contact.Address1_PostalCode = model.PostalCode;
            }

            var emailContent = new Incident
            {
                crspnd_IPAddress = Request.UserHostAddress,
                crspnd_MessageFormat = new OptionSetValue(675580005),
                crspnd_PortalSubject = model.Subject,
                crspnd_OriginalTopic = new EntityReference(Topic.EntityLogicalName, Guid.Parse("ecdce284-3fe1-e811-a956-001dd800c973")),
                crspnd_ReceivedDate = DateTime.SpecifyKind(DateTime.Now, DateTimeKind.Utc),
                crspnd_ResponseRequested = true,
                crspnd_Title = model.Subject,
                crspnd_Topic = new EntityReference(Topic.EntityLogicalName, Guid.Parse("ecdce284-3fe1-e811-a956-001dd800c973")),
                Description = model.Message,
                OwnerId = ServiceContext.ssc_controlpanelSet.FirstOrDefault()?.crspnd_DefaultCaseOwner
            };

            try
            {
                // ReSharper disable once RedundantAssignment
                var crmResult = ServiceContext.SaveChanges();

                // relate this incident to the contact
                emailContent.CustomerId = contact.ToEntityReference();

                ServiceContext.AddObject(emailContent);

                crmResult = ServiceContext.SaveChanges();

                if (foundContact && contact.StateCode.Value == ContactState.Inactive)
                {
                    var setStateRequest = new SetStateRequest
                    {
                        EntityMoniker = new EntityReference { Id = contact.Id, LogicalName = contact.LogicalName },
                        State = new OptionSetValue(0), // 0 = active, 1 = inactive
                        Status = new OptionSetValue(1) // 1 = active, 2 = inactive
                    };

                    ServiceContext.Execute(setStateRequest);
                }

                if (!crmResult.HasError)
                {
                    TempData["FormSuccess"] = true;
                    ModelState.Clear();
                    if (Request.IsAjaxRequest())
                    {
                        return PartialView(GetViewPath("ContactUsSuccess"));
                    }

                    TempData["JsDisabled"] = true;
                    return CurrentUmbracoPage();
                }
                else
                {
                    TempData["FormSuccess"] = false;
                    if (Request.IsAjaxRequest())
                    {
                        return PartialView(GetViewPath("ContactUsError"));
                    }

                    TempData["JsDisabled"] = true;
                    return CurrentUmbracoPage();
                }
            }
            catch (Exception ex)
            {
                message = "Exception caught - " + ex.Message;
                ((List<string>)TempData["CustomErrors"]).Add(message);
                TempData["FormSuccess"] = false;

                if(Request.IsAjaxRequest())
                {
                    return PartialView(GetViewPath("ContactUsError"));
                }

                TempData["JsDisabled"] = true;
                return CurrentUmbracoPage(); ;   
            }
        }
    }
}