﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace vadoc_public_website_redesign.Models
{
    public class ContactUsFormModel
    {
        [Required]
        public string FirstName { get; set; }

        [Required]
        public string LastName { get; set; }

        [Required]
        [EmailAddress(ErrorMessage = "A valid email address is required")]
        public string Email { get; set; }

        [DataType(DataType.PhoneNumber)]
        [RegularExpression(@"^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$", ErrorMessage = "Not a valid phone number")]
        public string Telephone { get; set; }

        public string AddressLine1 { get; set; }

        public string AddressLine2 { get; set; }

        public string AddressCity { get; set; }

        public string AddressState { get; set; }

        [RegularExpression(@"^\d{5}(-\d{4})?$", ErrorMessage = "Invalid Zipcode")]
        public string PostalCode { get; set; }

        [Required]
        public string Subject { get; set; }

        [Required]
        public string Message { get; set; }

        [Required]
        [RegularExpression(@"\briver\b",
         ErrorMessage = "Please enter the requested word or phrase.")]
        public string Captcha { get; set; }

        public bool RegardOffender { get; set; }

        public string OffenderFirstName { get; set; }

        public string OffenderLastName { get; set; }

        public string OffenderId{ get; set; }

        public string OffenderLocation{ get; set; }
    }
}