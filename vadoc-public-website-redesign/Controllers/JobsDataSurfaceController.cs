﻿using System.Collections.Generic;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using vadoc_public_website_redesign.Models;

namespace vadoc_public_website_redesign.Controllers
{
    public class JobsDataSurfaceController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/Jobs/";
        JobRepository JobRepo = new JobRepository();
        // GET: JobsDataSurface
        public ActionResult RenderJobsList()
        {

            List<Job> jobs = JobRepo.JobsList;
            TempData["JobsList"] = jobs;
            return PartialView($"{PARTIAL_VIEW_FOLDER}JobsList.cshtml", JobRepo);
        }
    }
}