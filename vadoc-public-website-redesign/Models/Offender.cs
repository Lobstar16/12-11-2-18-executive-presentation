﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace vadoc_public_website_redesign.Models
{
    public class Offender
    {
        public Offender()
        {
            this.FirstNameLowercase = null;
            this.LastNameLowercase = null;
            this.MiddleNameLowercase = null;
            this.FullName = null;
            this.Age = null;
            this.Race = null;
            this.Gender = null;
            this.ProjectedReleaseDate = null;
            this.LocationName = null;
            this.OffenderId = null;
            this.DOCResponsible = null;
        }

        public Offender(int? OffenderId, string FirstNameLowercase, string MiddleNameLowercase, string LastNameLowercase, string FullName, int? Age, string Gender, string Race, string ProjectedReleaseDate, string DOCResponsible, string LocationName)
        {
            this.FirstNameLowercase = FirstNameLowercase;
            this.LastNameLowercase = LastNameLowercase;
            this.MiddleNameLowercase = MiddleNameLowercase;
            this.FullName = FullName;
            this.Age = Age;
            this.Race = Race;
            this.Gender = Gender;
            this.ProjectedReleaseDate = ProjectedReleaseDate;
            this.LocationName = LocationName;
            this.OffenderId = OffenderId;
            this.DOCResponsible = DOCResponsible;
        }
        public string FirstNameLowercase { get; set; }
        public string MiddleNameLowercase { get; set; }
        public string LastNameLowercase { get; set; }
        public string FullName { get; set; }
        public int? Age { get; set; }
        public string Race { get; set; }
        public string Gender { get; set; }
        public string ProjectedReleaseDate { get; set; }
        public string LocationName { get; set; }
        public int? OffenderId { get; set; }
        public string DOCResponsible { get; set; }
    }
}