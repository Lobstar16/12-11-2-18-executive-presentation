// Globals

var openModal;
var modalIsOpen = false;
var closeModal;
var modalWindow;
var modalOverlay;
var modalOuter;
var modalContent;
var modalNav;
var modalNavLinks;
var modalCtrls;
var modalSlides;
var currentModalFocus;
var activeLink;
var lastFocus
var activeSlide;
var activeId;

// Modal UI
function setModalSlide(newActiveId) {
    modalNavLinks.removeClass('active');
    activeLink.find('img').attr('src', 'images/modals/step-1.svg');
    activeLink = $(modalNavLinks[newActiveId]);
    currentModalFocus = activeLink.find('a')[0];

    activeLink.addClass('active');
    activeLink.find('img').attr('src', 'images/modals/step-2.svg');
    $(activeSlide).hide(0, function () {
        activeSlide = modalSlides[newActiveId];
        $(activeSlide).show();
    });
}
function modalClose() {
    modalWindow.animate({
        top: '-=1300'
    }, 300)
    modalWindow.fadeOut(200);
    $(activeSlide).fadeOut(200);
    modalOverlay.fadeOut(200, function () {
        lastFocus.focus();
        modalIsOpen = false;
    });
}
function escModalClose(e) {
    if (!e.keyCode || e.keyCode === 27) {
        modalClose();
    }
}
function modalFocusRestrict(e) {
    document.addEventListener('focus', function (e) {
        if (modalIsOpen && !modalContent[0].contains(e.target) && !(modalCtrls[0].contains(e.target) || modalCtrls[1].contains(e.target)) && document.body.contains(e.target)) {
            e.stopPropagation();
            $(modalNavLinks[0]).find('a').focus();
        }
    }, true);
}

// Initialize
function initializeModal() {
    $.get('how-to-apply', function(res) {
        $('#current-job-openings').append(res);
    })
    .done(function() {
        modalWindow = $('.modal-window');
        modalOverlay = $('.modal-overlay');
        modalContent = $('.modal-content');
        openModal = $('#call-to-apply .secondary-button');
        modalNav = $('.modal-window nav .modal-nav');
        closeModal = $('.modal-content .close-modal');
        closeModal.click(function () {
            modalClose();
            $(document).off('keydown', escModalClose);
        });
        modalContent.click(function () {
            return false
        });

        //Add "How To Apply" modal open and close on click functionality 
        openModal.click(function (e) {
            e.preventDefault();
            e.stopPropagation();
            modalIsOpen = true;
            // Add keyboard accessibility to close modalhttps://www.nerdwallet.com/mortgages/how-much-house-can-i-afford/calculate-affordability
            $(document).on('keydown', escModalClose);
            // Save last focus element for screen readers
            lastFocus = document.activeElement;
            // If this is the first time modal has been opened, set modal defaults 
            if (!activeId) {
                activeId = 0;
                modalNavLinks = $('.modal-nav li');
                modalSlides = $('.modal-slides .slide');
                modalCtrls = $('.modal-ctrls li a');
                activeLink = $(modalNavLinks[0]);
                activeSlide = $(modalSlides[activeId]);
                $.each(modalSlides, function (index, slide) {
                    slide.id = index;
                });
                $.each(modalNavLinks, function (index, link) {
                    $(link).find('a').attr('href', index);
                    // Keyboard accessibility for navigating modal slides via modal nav links
                    $(link).on('focus', 'a', function (e) {
                        if (e.target !== currentModalFocus) {
                            e.preventDefault();
                            activeId = parseInt($(e.target).attr('href'));
                            currentModalFocus = e.target;
                            setModalSlide(activeId);
                        } else {
                            document.activeElement = currentModalFocus;
                        }
                    });
                });
                $([modalOverlay.get(0),
                modalWindow.get(0)]
                ).on('click', function (e) {
                    modalClose();
                });
                // Modal next and previous slide controls
                modalCtrls.off('click');
                modalCtrls.on('click', function (e) {
                    e.preventDefault();
                    e.stopPropagation();
                    activeId += parseInt($(this).attr('data-val'));
                    if (activeId > modalNavLinks.length - 1) {
                        activeId = 0;
                    } else if (activeId < 0) {
                        activeId = modalNavLinks.length - 1;
                    }
                    setModalSlide(activeId);
                });
                modalFocusRestrict();
            }
            // Fade in modal
            modalOverlay.fadeIn(0, function () {
                modalWindow.fadeIn(300);
                modalWindow.animate({
                    top: '0'
                }, 300);
                activeLink.find('a').focus();
                setModalSlide(activeId);
            });
        });
    });
}

$(function () {
    initializeModal();
});