$(function() {
	var zipVal;
	var lat;
	var lng;
	var zipInput = $('#zipcode');
	var geocoder = new google.maps.Geocoder();

	function handleError(status) {
		console.log(status);
	}

	function isInt(value) {
		if((parseFloat(value) == parseInt(value)) && !isNaN(value)) {
			return true;
		} else {
			return false;
		}
	};

	// Autofill city and us-state inputs with parsed city and state values
	function populateAddressFields(city, state, country) {
		$('#city').val(city);
		$('#us-state').val(state);
		$('#country').val(country);
	};

	// Get geocode locations associated with zipcode via google maps API
	function getLocation(zip) {
		geocoder.geocode({'address': zip}, function( results, status) {
			if(status == google.maps.GeocoderStatus.OK) {
				geocoder.geocode({'latLng': results[0].geometry.location}, function( results, status) {
					if(status == google.maps.GeocoderStatus.OK) {
						if(results[1]) {
							getCityState(results[0]);
						}
					}
				});
			} else {
				// Zero results found
				handleError(status);
			}; 
		});
	};

	// Parse City and State names from geocode location data
	function getCityState(location) {
		var city, state, country;
		var address = location.address_components;

		for (var i = 0; i < address.length; i++) {
			if(address[i].types.includes('locality')) {
				city = address[i].long_name;
			} else if(address[i].types.includes('administrative_area_level_1')) {
				state = address[i].long_name;
			} else if(address[i].types.includes('sublocality_level_1') && (city == undefined)) {
				city = address[i].long_name;
			} else if(address[i].types.includes('country')) {
				country = address[i].long_name;
			}

		} 
		populateAddressFields(city, state, country);
	};

	zipInput.keyup(function(e) {
		zipVal = $(e.target).val();

		// Check for valid zip code entry
		if((zipVal.length === 5) && (isInt(zipVal))) {
			getLocation(zipVal);
		}
	});
});