﻿using System.Web.Http;
using System.Web.Http.Cors;

namespace vadoc_public_website_redesign.App_Start
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            // Allow origin requests from Visitation Application 
            var corsAttr = new EnableCorsAttribute("http://localhost:3000", "*", "*");
            config.EnableCors(corsAttr);
        }
    }
}