var isMobile = false;

$(function() {
	var mainNav = $('#main-nav');
	var articleTexts = $('.article-text p:not(.date-stamp)');

	if(mainNav.css('display') == 'none') {
		isMobile = true;
	}
	if(isMobile) {
		truncateArticleText();
	}
	function truncateArticleText() {
		$.each(articleTexts, function(index, textContent) {
			if ($(textContent).text().length > 60) {
				var truncated = $(textContent).text().substring(0, 60) + '...';
				$(textContent).text(truncated);
			}
		});
	}
});