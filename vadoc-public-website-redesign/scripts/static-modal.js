// each modal trigger has id 
// modal content div has associated trigger id
// one modal overlay/display
// On click modal triggers:
	// set active content id
	// empty contents of modal
	// find contents of active id
	// append contents to modal display
	// show modal
// ---------------
// Globals
// ---------------
var activeId;
var currentModalInstance;
var modalContent;
var modalInstances;
var modalTriggers;
var modalOverlay;
var modalWindow;
var lastFocus;
var closeModal;
var modalIsOpen = false;

// ----------------
// Modal functions
// ----------------
// set new active modal id
function setActiveId(id) {
	activeId = id;
}
// set new modal instance
function setCurrentInstance() {
	for(var i = 0; i < modalInstances.length; i++) {
		var instance = modalInstances[i];
		if(instance.id === activeId) {
			currentModalInstance = $(instance).clone();
		}
	}
}
// clear current modal content
function clearModalContent() {
	$('.modal-content').find('.modal-instance').remove();
}
// set new modal content
function setModalContent() {
	modalContent.append(currentModalInstance);
}
// show modal
function showModal() {
	modalOverlay.fadeIn(0, function () {
        modalWindow.fadeIn(300);
        modalWindow.animate({
            top: '0'
        }, 300);
        $(currentModalInstance).show();
    });
}
// close modal
function modalClose() {
    modalWindow.animate({
        top: '-=1300'
    }, 300)
    modalWindow.fadeOut(200);
    modalOverlay.fadeOut(200, function () {
        lastFocus.focus();
        modalIsOpen = false;
    });
}
// close modal with keyboard
function escModalClose(e) {
    if (!e.keyCode || e.keyCode === 27) {
        modalClose();
        $(document).off('keydown', escModalClose);
    }
}
// restrict focus to modal elements only
function modalFocusRestrict(e) {
    document.addEventListener('focus', function (e) {
        if (modalIsOpen && document.body.contains(e.target)) {
            e.stopPropagation();
            closeModal.focus();
        }
    }, true);
}
// ----------------
// Document Ready
// ----------------
$(function() {
	// get all modal triggers
	modalTriggers = $('.modal-trigger');
	// get all modal instances
	modalInstances = $('.modal-instance');
	// set modal display elements
	modalOverlay = $('.modal-overlay');
	modalWindow = $('.modal-window');
	modalContent = $('.modal-content');
	closeModal = $('.modal-content .close-modal');
    // Assign close modal click handler
    closeModal.click(function () {
        modalClose();
        $(document).off('keydown', escModalClose);
    });
	// Assign modal triggers click handler
	modalTriggers.on('click', function(e) {
		e.preventDefault();
		e.stopPropagation();
		modalIsOpen = true;
		// Add keyboard accessibility to close modalhttps://www.nerdwallet.com/mortgages/how-much-house-can-i-afford/calculate-affordability
        $(document).on('keydown', escModalClose);
        // Save last focus element for screen readers
        lastFocus = document.activeElement;
		
		var trigger = this;
		var id = $(trigger).attr('data-id');
		
		clearModalContent();
		setActiveId(id);
		setCurrentInstance();
		setModalContent();
		showModal();
		modalFocusRestrict();
	})
});
