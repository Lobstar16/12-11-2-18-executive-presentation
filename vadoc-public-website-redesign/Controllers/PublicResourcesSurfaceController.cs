﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using vadoc_public_website_redesign.Models;

namespace vadoc_public_website_redesign.Controllers
{
    public class PublicResourcesSurfaceController : SurfaceController
    {
        private const string MAIN_VIEW_FOLDER = "~/Views/";
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/OffenderLocator/";
        // GET: Offender Locator Page
        public ActionResult RenderOffenderLocatorPage()
        {
            return Redirect("/public-resources/offender-locator");
        }
        public ActionResult RenderOffenderLocatorForm()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}OffenderLocatorForm.cshtml");
        }

        public ActionResult RenderOffenderSearchResults()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}OffenderSearchResults.cshtml");
        }

        public ActionResult RenderOffenderSearchNoResults()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}OffenderSearchNoResults.cshtml");
        }
    }
}