var lastFocus;

$(function() {
	var imageSliders = $('.image-slider');
	var modalWindow = $('.modal-window');
	// Image slider object instantiation
	function Slider(id, slides) {
		this.id = id;
		this.domNode = document.getElementById(id);
		// Slider controls
		this.nextCtrl = $(this.domNode).find('.next');
		this.previousCtrl =  $(this.domNode).find('.previous');
		this.expandCtrl = $(this.domNode).find('.expand');
		// Slider images
		this.slides = slides;
		this.slideTotal = slides.length;
		this.numberOfSlides = slides.length;
		this.currentSlide = this.slides[0];
		this.currentSlideNumber = parseInt($(this.currentSlide).attr('data-slide-number'));
		this.currentSlideImageHeight = $(this.currentSlide).find('img').height();
		this.currentSlideImageWidth = $(this.currentSlide).find('img').width();
		this.bubbleLinks = $(this.domNode).find('.slider-bubble-link');
		this.sliderBubbleNav = $(this.domNode).find('.image-slider-ctrls').find('.slider-bubble-nav')[0];
		//Slider Layout 
		this.setSliderCtrls = function() {
			$(this.nextCtrl).css('top', (this.currentSlideImageHeight/2) - ($(this.nextCtrl).height()/2) + 20);
			$(this.previousCtrl).css('top', (this.currentSlideImageHeight/2) - ($(this.previousCtrl).height()/2) + 20);	
			$(this.expandCtrl).css('top', (this.currentSlideImageHeight) - ($(this.expandCtrl).find('img').height()) + 20);
			$(this.sliderBubbleNav).css({
				top: (this.currentSlideImageHeight + ($(this.sliderBubbleNav).height() * 1.5) + 22),
				left: (this.currentSlideImageWidth/2) - ($(this.sliderBubbleNav).width()/2)
			});
		}
		this.hoverIn = function() {
			$(this.nextCtrl).find('img').attr('src', 'images/image-slider/image-nav-50.svg');
			$(this.previousCtrl).find('img').attr('src', 'images/image-slider/image-nav-50.svg');
			$(this.expandCtrl).find('img').attr('src', 'images/image-slider/block-expand-50.svg');
		}
		this.hoverOut =	function() {
			$(this.nextCtrl).find('img').attr('src', 'images/image-slider/image-nav-25.svg');
			$(this.previousCtrl).find('img').attr('src', 'images/image-slider/image-nav-25.svg');
			$(this.expandCtrl).find('img').attr('src', 'images/image-slider/block-expand-25.svg');
		}
		//Slider modal window
		this.modalOverlay = $(this.domNode).find('.modal-overlay');
		this.modalWindow = $(this.domNode).find('.modal-window');
		this.modalCtrls = $(this.domNode).find('nav.modal-ctrls');
		this.modalIsOpen = false;
		this.closeModalCtrl = $(this.domNode).find('.close-modal');
		this.currentModalSlide;
		this.modalContent = $(this.modalWindow).find('.modal-content');
		this.setCurrentModalSlide = function() {
			this.currentModalSlide = $(this.currentSlide).clone();
			$(this.currentModalSlide).css('margin-bottom', '2em');
			$(this.currentModalSlide).find('img').css('margin-bottom', '0')
		} 
		this.hideCurrentModalSlide = function() {
			var contentWindow = $(this.modalWindow).find('.modal-content')[0];
			$(this.currentModalSlide).empty();
			$(this.currentModalSlide).remove();
		}
		this.showCurrentModalSlide = function() {
			this.modalContent.prepend(this.currentModalSlide);
		}
		// Modal Controls
		this.modalNextCtrl = $(this.modalWindow).find('.modal-ctrl-next');
		this.modalPrevCtrl = $(this.modalWindow).find('.modal-ctrl-prev');
		// Slider Functionality
		this.setCurrentSlide = function(slide) {
			this.currentSlide = slide;
		}
		this.hideCurrentSlide = function() {
			$(this.currentSlide).hide()
		}
		this.showCurrentSlide = function() {
			$(this.currentSlide).show()
			this.currentSlideImageHeight = $(this.currentSlide).find('img').height();
			this.currentSlideImageWidth = $(this.currentSlide).find('img').width();
			this.setSliderCtrls();
		}
		this.nextSlide = function() {
			if((this.currentSlideNumber + 1) < this.slideTotal) {
				this.currentSlideNumber += 1;
			} else {
				this.currentSlideNumber = 0;	
			}
			this.hideCurrentSlide();
			this.setCurrentSlide($(this.domNode).find("[data-slide-number='" + this.currentSlideNumber + "']"));
			this.showCurrentSlide();
			this.setActiveBubbleLink();
			
			if(this.modalIsOpen) {
				this.hideCurrentModalSlide();
				this.setCurrentModalSlide();
				this.showCurrentModalSlide();
			}
		}
		this.previousSlide = function() {
			if((this.currentSlideNumber - 1) < 0) {
				this.currentSlideNumber = this.slideTotal - 1;
			} else {
				this.currentSlideNumber -= 1;	
			}
			this.hideCurrentSlide();
			this.currentSlide = $(this.domNode).find("[data-slide-number='" + this.currentSlideNumber + "']");
			this.showCurrentSlide();
			this.setActiveBubbleLink();

			if(this.modalIsOpen) {
				this.hideCurrentModalSlide();
				this.setCurrentModalSlide();
				this.showCurrentModalSlide();
			}
		}
		this.handleBubbleNav = function(slideNumber) {
			this.currentSlideNumber = parseInt(slideNumber);
			this.hideCurrentSlide();
			this.currentSlide = $(this.domNode).find("[data-slide-number='" + this.currentSlideNumber + "']");
			this.showCurrentSlide();
			this.setActiveBubbleLink();

			if(this.modalIsOpen) {
				this.hideCurrentModalSlide();
				this.setCurrentModalSlide();
				this.showCurrentModalSlide();
			}
		}
		this.setActiveBubbleLink = function() {
			$(this.domNode).find('a.slider-bubble-link').removeClass('active-bubble-link');
			$(this.domNode).find('a[href="' + this.currentSlideNumber + '"]').addClass('active-bubble-link');
		}
		this.openModal = function(slider) {
			slider.modalIsOpen = true;
			// Add keyboard accessibility to close modal
			$(document).on('keydown', slider.escModalClose.bind(slider));
			// Save last focus element for screen readers
			slider.lastFocus = document.activeElement;
			// Fade in modal overlay and modal window
			slider.modalOverlay.fadeIn(200, function() {
				var modalBubbleNav = $(slider.domNode).find('.modal-content .slider-bubble-nav ul')[0];
				modalWindow.animate({
					top: '0'
				}, 300);
				$(slider.modalCtrls).hide();
				slider.setCurrentModalSlide();
				slider.modalWindow.fadeIn(300, function() {
					$(slider.modalCtrls).css('top', ($(slider.currentModalSlide).find('img').height() * 0.5) - 30);
					$(slider.modalCtrls).fadeIn(200);
				});
				slider.modalWindow.css('filter', 'blur(0)');
				slider.modalContent.prepend(slider.currentModalSlide);
				slider.modalContent.click(function(e) {
					e.stopPropagation();
					e.preventDefault();
				})
			});
		}
		this.modalClose = function(slider) {
			var slider = this;
			this.modalWindow.css('filter', 'blur(8px)');
			this.modalWindow.fadeOut(200, function() {
				slider.hideCurrentModalSlide();
			});
			this.modalOverlay.fadeOut(200, function() {
				slider.lastFocus.focus();
				slider.modalIsOpen = false;
			});
		}
		this.escModalClose = function escModalClose(e) {
			if(!e.keyCode || e.keyCode === 27) {
				this.modalClose(this);
			}
		}
	}
	// Instantiate image sliders
	$.each(imageSliders, function(index, slider) {
		var sliderId = 'image-slider-' + index.toString() 
		var slides = $(slider).find('figure');
		var sliderBubbleNav = $(slider).find('.slider-bubble-nav ul');
		$.each(slides, function(index, slide) {
			var bubbleNavListItem = "<li><a class='slider-bubble-link' href='" + index +"'></a></li>";
			$(slide).attr('data-slide-number', index);
			$.each(sliderBubbleNav, function(index, bubbleNav) {
				$(bubbleNav).append(bubbleNavListItem);
			});
		});
		slider.id = sliderId;
		var slider = new Slider(sliderId, slides);
		// Add slider functionality
		$([slider.nextCtrl.get(0),
			slider.modalNextCtrl.get(0)]
		).click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			slider.nextSlide();
		});
		$([slider.previousCtrl.get(0), 
			slider.modalPrevCtrl.get(0)]
		).click(function(e) {
			e.stopPropagation();
			e.preventDefault();
			slider.previousSlide();
		});
		$([slider.modalOverlay.get(0), 
			slider.modalWindow.get(0)]
		).on('click',function(e) {
			slider.modalClose(slider);
		});
		$(slider.expandCtrl).click(function(e) {
			e.preventDefault();
			slider.openModal(slider);
		})
		$.each(slider.bubbleLinks, function(index, link) {
			$(link).click(function(e) {
				var slideNumber = $(link).attr('href');
				e.stopPropagation();
				e.preventDefault();
				slider.handleBubbleNav(slideNumber);
			})
		});
		$(slider.closeModalCtrl).click(function() {
			slider.modalClose(slider);
		})
		$(slider.domNode).find('ul.image-slides').find('img').css('margin-bottom', $(slider.sliderBubbleNav).height() + 35.5);
		slider.setSliderCtrls();
		$(slider.slides).find('img').hover(function() {
				slider.hoverIn();
			},
			function() {
				slider.hoverOut(); 
			}
		)
		$([
			slider.nextCtrl.get(0),
			slider.previousCtrl.get(0),
			slider.expandCtrl.get(0)
		]
		).hover(function() {
				slider.hoverIn();
			},
			function() {
				slider.hoverOut(); 
			}
		)
		slider.setActiveBubbleLink();
	})
})