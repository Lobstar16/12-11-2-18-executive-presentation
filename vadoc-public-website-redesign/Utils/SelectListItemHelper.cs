﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using vadoc_public_website_redesign.Crm;
using System.Web.Mvc;
using Umbraco.Web.Mvc;
using System.Configuration;
using Microsoft.Xrm.Sdk;
using Microsoft.Xrm.Sdk.Client;
using Microsoft.Crm.Sdk.Messages;
using System.Net;
using System.ServiceModel.Description;

namespace vadoc_public_website_redesign.Utils
{
    public class SelectListItemHelper
    {
        private readonly string _crmServiceAccountUserName =
           ConfigurationManager.AppSettings["CrmServiceAccountUserName"];
        private readonly string _crmServiceAccountPassword =
            ConfigurationManager.AppSettings["CrmServiceAccountPassword"];
        private readonly string _crmServiceAccountUrl =
            ConfigurationManager.AppSettings["CrmServiceUrl"];
        private const string Status = "statuscode";

        private DocServiceContext ServiceContext { get; set; }

        ClientCredentials clientCredentials = new ClientCredentials();

        public static IEnumerable<SelectListItem> GetStatesList()
        {
            IList<SelectListItem> items = new List<SelectListItem>
            {
                new SelectListItem {Text = "ALABAMA", Value = "AL"},
                new SelectListItem {Text = "ALASKA", Value = "AK"},
                new SelectListItem {Text = "AMERICAN SAMOA", Value = "AS"},
                new SelectListItem {Text = "ARIZONA", Value = "AZ"},
                new SelectListItem {Text = "ARKANSAS", Value = "AR"},
                new SelectListItem {Text = "CALIFORNIA", Value = "CA"},
                new SelectListItem {Text = "COLORADO", Value = "CO"},
                new SelectListItem {Text = "CONNECTICUT", Value = "CT"},
                new SelectListItem {Text = "DELAWARE", Value = "DE"},
                new SelectListItem {Text = "DISTRICT OF COLUMBIA", Value = "DC"},
                new SelectListItem {Text = "FEDERATED STATES OF MICRONESIA", Value = "FM"},
                new SelectListItem {Text = "FLORIDA", Value = "FL"},
                new SelectListItem {Text = "GEORGIA", Value = "GA"},
                new SelectListItem {Text = "GUAM", Value = "GU"},
                new SelectListItem {Text = "HAWAII", Value = "HI"},
                new SelectListItem {Text = "IDAHO", Value = "ID"},
                new SelectListItem {Text = "ILLINOIS", Value = "IL"},
                new SelectListItem {Text = "INDIANA", Value = "IN"},
                new SelectListItem {Text = "IOWA", Value = "IA"},
                new SelectListItem {Text = "KANSAS", Value = "KS"},
                new SelectListItem {Text = "KENTUCKY", Value = "KY"},
                new SelectListItem {Text = "LOUISIANA", Value = "LA"},
                new SelectListItem {Text = "MAINE", Value = "ME"},
                new SelectListItem {Text = "MARSHALL ISLANDS", Value = "MH"},
                new SelectListItem {Text = "MARYLAND", Value = "MD"},
                new SelectListItem {Text = "MASSACHUSETTS", Value = "MA"},
                new SelectListItem {Text = "MICHIGAN", Value = "MI"},
                new SelectListItem {Text = "MINNESOTA", Value = "MN"},
                new SelectListItem {Text = "MISSISSIPPI", Value = "MS"},
                new SelectListItem {Text = "MISSOURI", Value = "MO"},
                new SelectListItem {Text = "MONTANA", Value = "MT"},
                new SelectListItem {Text = "NEBRASKA", Value = "NE"},
                new SelectListItem {Text = "NEVADA", Value = "NV"},
                new SelectListItem {Text = "NEW HAMPSHIRE", Value = "NH"},
                new SelectListItem {Text = "NEW JERSEY", Value = "NJ"},
                new SelectListItem {Text = "NEW MEXICO", Value = "NM"},
                new SelectListItem {Text = "NEW YORK", Value = "NY"},
                new SelectListItem {Text = "NORTH CAROLINA", Value = "NC"},
                new SelectListItem {Text = "NORTH DAKOTA", Value = "ND"},
                new SelectListItem {Text = "NORTHERN MARIANA ISLANDS", Value = "MP"},
                new SelectListItem {Text = "OHIO", Value = "OH"},
                new SelectListItem {Text = "OKLAHOMA", Value = "OK"},
                new SelectListItem {Text = "OREGON", Value = "OR"},
                new SelectListItem {Text = "PALAU", Value = "PW"},
                new SelectListItem {Text = "PENNSYLVANIA", Value = "PA"},
                new SelectListItem {Text = "PUERTO RICO", Value = "PR"},
                new SelectListItem {Text = "RHODE ISLAND", Value = "RI"},
                new SelectListItem {Text = "SOUTH CAROLINA", Value = "SC"},
                new SelectListItem {Text = "SOUTH DAKOTA", Value = "SD"},
                new SelectListItem {Text = "TENNESSEE", Value = "TN"},
                new SelectListItem {Text = "TEXAS", Value = "TX"},
                new SelectListItem {Text = "UTAH", Value = "UT"},
                new SelectListItem {Text = "VERMONT", Value = "VT"},
                new SelectListItem {Text = "VIRGIN ISLANDS", Value = "VI"},
                new SelectListItem {Text = "VIRGINIA", Value = "VA", Selected = true},
                new SelectListItem {Text = "WASHINGTON", Value = "WA"},
                new SelectListItem {Text = "WEST VIRGINIA", Value = "WV"},
                new SelectListItem {Text = "WISCONSIN", Value = "WI"},
                new SelectListItem {Text = "WYOMING", Value = "WY"}
            };

            return items;
        }

        public IEnumerable<SelectListItem> PopulateFacilities()
        {
            List<SelectListItem> items = new List<SelectListItem> { new SelectListItem { Text = "Select One", Value = "" } };

            try
            {
                ClientCredentials clientCredentials = new ClientCredentials();
                clientCredentials.UserName.UserName = _crmServiceAccountUserName;
                clientCredentials.UserName.Password = _crmServiceAccountPassword;

                // For Dynamics 365 Customer Engagement V9.X, set Security Protocol as TLS12
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
                var organizationServiceProxy =
                new OrganizationServiceProxy(new Uri(_crmServiceAccountUrl), null,
                    clientCredentials, null);
                organizationServiceProxy.EnableProxyTypes();

                ServiceContext = new DocServiceContext(organizationServiceProxy);
            }
            catch
            {
                ServiceContext = null;
            }
            if (ServiceContext != null)
            {
                items.AddRange((from s in ServiceContext.crspnd_facilitySet
                                where s.GetAttributeValue<OptionSetValue>(Status).Value == 1
                                orderby s.crspnd_name
                                select new SelectListItem { Text = s.crspnd_name, Value = s.Id.ToString() }).ToArray());
            }

            return items;
        }

    }
}