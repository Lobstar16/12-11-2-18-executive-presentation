﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace vadoc_public_website_redesign.Models
{
    public class OffenderSearchModel : IValidatableObject
    {
        public int? OffenderId { get; set; }
        private string firstName;
        public string FirstNameLowercase
        {
            get { return firstName; }
            set { firstName = value?.Trim().ToLower(); }
        }
        private string middleName;
        public string MiddleNameLowercase
        {
            get { return this.middleName; }
            set { this.middleName = value?.Trim().ToLower(); }
        }
        private string lastName;
        public string LastNameLowercase
        {
            get { return lastName; }
            set { lastName = value?.Trim().ToLower(); }
        }
        public string Alias { get; set; }
        [DataType(DataType.Date)]
        public string ReleaseDate { get; set; }
        private string race;
        public string Race
        {
            get { return race; }
            set { race = value?.Trim().ToLower(); }
        }
        private string gender;
        public string Gender
        {
            get { return gender; }
            set { gender = value?.Trim().ToLower(); }
        }
        [Range(18, 100)]
        public int? AgeFrom { get; set; }
        [Range(18, 100)]
        public int? AgeTo { get; set; }

        public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
        {
            if((OffenderId == null && string.IsNullOrEmpty(LastNameLowercase)) || (OffenderId == null && string.IsNullOrEmpty(FirstNameLowercase)))
            {
                yield return new ValidationResult("A first and last name OR Offender I.D. # are required for all searches.", new List<string> { "OffenderId", "LastNameLowercase", "FirstNameLowercase" });
            }
        }
    }
}