$(function() {
	var domains = [
		"yahoo.com",
		"gmail.com", 
		"google.com", 
		"hotmail.com", 
		"me.com", 
		"aol.com", 
		"mac.com", 
		"live.com", 
		"comcast.com", 
		"googlemail.com", 
		"msn.com", 
		"hotmail.co.uk", 
		"yahoo.co.uk", 
		"facebook.com", 
		"verizon.net", 
		"att.net", 
		"gmz.com", 
		"mail.com"
	]

	$('#email').autocomplete({
		source: [],
		minChars: 0,
		matchSubset: false,
		matchContains: true
	})

	$('#email').keyup(function(e) {
		userInput = $(this).val();

		if(userInput.includes('@')) {
			var targetPos = userInput.indexOf('@');
			var prefix = userInput.substring(0, targetPos + 1);
			var emailSugg = [];

			for(var i = 0; i < domains.length; i++) {
				emailSugg.push(prefix + domains[i]);
			}

			$('#email').autocomplete({
				source: emailSugg,
				minChars: 0,
				matchSubset: false,
				matchContains: true,
			});
		} else {
			$('#email').autocomplete({
				source: [],
				minChars: 0,
				matchSubset: false,
				matchContains: true
			});
		}
		
		
	});
	$('#us-state').editableSelect({
		effects: 'fade',
		duration: 200	
	});
	$('#country').editableSelect({
		effects: 'fade',
		duration: 200	
	});
});