$(function() {
	//-------------------------
	// General Site UI
	// ------------------------
	var windowTop = $(window).scrollTop();
	var body = document.body;
	var bodyTop = body.getBoundingClientRect().top;
	var footer = document.getElementById('main-footer');
	var elevator = document.getElementById('back-to-top');
	var elevatorTop = elevator.getBoundingClientRect().top;
	var circle = $('#circle');
	var chevron = $('#chevron');
	var transitionPt = elevatorTop + 30;
    
    //Determine if i.e. and what version 
    function msieversion() {
        var rv = -1; // Return value assumes failure.

        if (navigator.appName == 'Microsoft Internet Explorer'){

            var ua = navigator.userAgent,
               re  = new RegExp("MSIE ([0-9]{1,}[\\.0-9]{0,})");

            if (re.exec(ua) !== null){
             rv = parseFloat( RegExp.$1 );
            }
        }
        else if(navigator.appName == "Netscape"){                       
           /// in IE 11 the navigator.appVersion says 'trident'
           /// in Edge the navigator.appVersion does not say trident
           if(navigator.appVersion.indexOf('Trident') === -1) rv = 12;
           else rv = 11;
        }       

        return rv;
    }
    ////////////////
    // Check if mobile device
    function checkMobile() {
        return (Modernizr.touchevents
                && screen && screen.width < 700
            );
    }
    //////////////
	function isScrolledToFooter(footerTop) {
		return ((!circle.hasClass('fill-white') && footerTop <= transitionPt) || (circle.hasClass('fill-white') && footerTop > transitionPt));		
	}
	function transitionClass() {
		if(!circle.hasClass('fill-white')) {
			circle.addClass('fill-white');
			chevron.addClass('stroke-white');
		} else {
			circle.removeClass('fill-white');
			chevron.removeClass('stroke-white');
		}
	}
	function initialize() {
        // jQuery fix for placeholder text in i.e. 8-9
        $('[placeholder]').focus(function() {
          var input = $(this);
          if (input.val() == input.attr('placeholder')) {
            input.val('');
            input.removeClass('placeholder');
          }
        }).blur(function() {
          var input = $(this);
          if (input.val() == '' || input.val() == input.attr('placeholder')) {
            input.addClass('placeholder');
            input.val(input.attr('placeholder'));
          }
        }).blur().parents('form').submit(function() {
          $(this).find('[placeholder]').each(function() {
            var input = $(this);
            if (input.val() == input.attr('placeholder')) {
              input.val('');
            }
          })
        });
        //Add auto scroll functionality to return-top button
		$(elevator).click(function(event) {
			event.preventDefault();
			autoScroll.toId(body.id, 20);
		});
        //On scroll check to see if return-top button is over dark footer and change color accordingly
		if(footer) {
            window.onscroll = function() {
    			var footerTop = footer.getBoundingClientRect().top;
    			if (isScrolledToFooter(footerTop)) {
    				transitionClass();
    			}

                 var y =$(window).scrollTop();

                if (y > 300)
                {
                    $(elevator).fadeIn(200);    
                } else {
                    setTimeout(function() {
                        $(elevator).fadeOut(200);
                    });
                }
    		}
        }
        $(elevator).hide();
		window.scrollTo(0, windowTop - 1);
	}
	//-------------------------
	// Auto-Scroll UI
	//-------------------------
        var autoScroll = {
            documentVerticalScrollPosition: function () {
                if (self.pageYOffset) return self.pageYOffset; // Firefox, Chrome, Opera, Safari
                if (document.documentElement && document.documentElement.scrollTop) return document.documentElement.scrollTop; // Internet Explorer 6
                if (document.body.scrollTop) return document.body.scrollTop; // Internet Explorer 6, 7 and 8
                return 0; // None of the above
            },
            viewportHeight: function () {
                return (document.compatMode === "CSS1Compat") ? document.documentElement.clientHeight : document.body.clientHeight;
            },
            documentHeight: function () {
                return (document.height !== undefined) ? document.height : document.body.offsetHeight;
            },
            documentMaximumScrollPosition: function () {
                return this.documentHeight() - this.viewportHeight();
            },
            elementVerticalClientPositionById: function (id) {
                var element = document.getElementById(id);
                var rectangle = element.getBoundingClientRect();
                return rectangle.top;
            },
            targetId: null,
            targetPadding: 0,
            isScrolling: false,
            killScroll: false,
            //Animation
            scrollVerticalTickToPosition: function (currentPosition, targetPosition, id, padding) {
                if (this.killScroll) {
                    this.killScroll = false;
                    this.isScrolling = false;
                    this.toId(this.targetId, this.targetPadding);   
                } else {
                    this.isScrolling = true;
                    var filter = 0.07;
                    var fps = 80;
                    var difference = parseFloat(targetPosition) - parseFloat(currentPosition);
                    var arrived = (Math.abs(difference) <= 0.5);
                    if (arrived) {
                        scrollTo(0.0, targetPosition);
                        this.isScrolling = false;
                        return;
                    }
                    currentPosition = (parseFloat(currentPosition) * (1.0 - filter)) + (parseFloat(targetPosition) * filter);
                    scrollTo(0.0, Math.round(currentPosition));
                    setTimeout(function () { autoScroll.scrollVerticalTickToPosition(currentPosition, targetPosition, id, padding) }, (1000 / fps));
                }
            },
            toId: function (id, padding) {
                this.targetId = id;
                this.targetPadding = padding;
                if (this.isScrolling) {
                    this.killScroll = true;
                } else {
                    var element = document.getElementById(id);
                    if (element == null) {
                        //console.warn('Cannot find element with id \''+id+'\'.');
                        return;
                    }
                    var targetPosition = this.documentVerticalScrollPosition() + this.elementVerticalClientPositionById(id) - padding;
                    var currentPosition = this.documentVerticalScrollPosition();
                    var maximumScrollPosition = this.documentMaximumScrollPosition();
                    if (targetPosition > maximumScrollPosition) targetPosition = maximumScrollPosition;
                    this.scrollVerticalTickToPosition(currentPosition, targetPosition, id, padding);
                }
            }
        };
	// -----------------------------------------
    //-------------------------
    // Dual Range Slider UI
    //-------------------------
    function updateRangeDisplay(slider, value) {
        var rangeDisplay = $(slider).next('.range-display')[0];
        var low = $(rangeDisplay).find('.range-low');
        var high = $(rangeDisplay).find('.range-high');
        var lowInput = $(slider).find('.slider-from').find('input');
        var highInput = $(slider).find('.slider-to').find('input');

        $(rangeDisplay).show()

        $(low).text($(slider).slider("values", 0));
        $(lowInput).val($(slider).slider("values", 0))
        $(high).text($(slider).slider("values", 1));
        $(highInput).val($(slider).slider("values", 1))
    }
    if(checkMobile()) {
        var rangeSliders = $('.dual-range-slider');
        if (rangeSliders.length > 0) {
            $(rangeSliders).slider({
                min: 18,
                max: 99,
                range: true,
                values: [18, 35],
                slide: function(){updateRangeDisplay(this)},
                change: function(){updateRangeDisplay(this)}
            });
        }

        $.each(rangeSliders, function(index, slider) {
            $(slider).find('.slider-from, .slider-to').hide();
            $(slider).show();
            updateRangeDisplay(slider);
        })
    }
    
    //------------------------------------------
    //-------------------------
    // Datepicker UI
    //-------------------------
    var datePickers = $('.has-datepicker'); 
    var datepickerTriggers = $('.calendar-icon');
    if(checkMobile()) {
        $.each(datePickers, function(index, datepicker) {
            datepicker.type = 'date';
        })
    } else {
        $(datepickerTriggers).click(function(e) {
            $(e.target).siblings('.has-datepicker').datepicker("show");
        })
        if(datePickers.length > 0) {
            $(datePickers).datepicker();

            $('.calendar-icon').datepicker({
                showOn: "button",
                buttonImageOnly: true,
                buttonImage: "../images/benefit-icons/calendar.svg",
                buttonText: "Calendar",
            })

            $.datepicker.setDefaults({ 
                yearRange: "2000:3012",
                changeYear: true
            })
        }    
    }
    //--------------------------
	initialize();
})