﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Umbraco.Web.Mvc;
using System.Web.Mvc;

namespace vadoc_public_website_redesign.Controllers
{
    public class SIteLayoutController : SurfaceController
    {
        private const string PARTIAL_VIEW_FOLDER = "~/Views/Partials/SiteLayout/";
        public ActionResult RenderHeader()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}Header.cshtml");
        }

        public ActionResult RenderFooter()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}Footer.cshtml");
        }

        public ActionResult RenderReturnTop()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}ReturnToTop.cshtml");
        }

        public ActionResult RenderSecondaryNav()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}SecondaryNav.cshtml");
        }

        public ActionResult RenderPrimaryNav()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}PrimaryNav.cshtml");
        }
        public ActionResult RenderMobileNav()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}MobileNav.cshtml");
        }

        public ActionResult RenderHomeHero()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}HomeHero.cshtml");
        }

        public ActionResult RenderBasicHero()
        {
            return PartialView($"{PARTIAL_VIEW_FOLDER}BasicHero.cshtml");
        }
    }
}