$(function() {
	var mostWantedCards = $('.most-wanted-card');

	for(var i = 0; i < mostWantedCards.length; i += 2) {
		var cardRow = mostWantedCards.slice(i, i + 2);
		var firstCard = $(cardRow[0]).find('.mw-card-right');
		var secondCard = $(cardRow[1]).find('.mw-card-right');
		
		$(firstCard).height() > $(secondCard).height() ?  
			$(secondCard).css('min-height', $(firstCard).height()) :
			$(firstCard).css('min-height', $(secondCard).height());
	}
});

