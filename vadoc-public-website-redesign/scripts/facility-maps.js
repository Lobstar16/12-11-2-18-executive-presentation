
var defaults = {
        data: {"regions":["Western","Central","Eastern"],"facilityTypes":["prison","probation and parole","community facility","secure medical facility","a starbucks"],"facilities":{"1":{"name":"Augusta","type":"prison","region":"1","phone":"(540) 997-7000","address":{"street":"1821 Estaline Valley Road","city":"Craigsville","zip":"24430"},"coords":{"lat":"38.0592644","long":"-79.3658322"},"Warden":"John A. Woodson","securityLevel":"3","population":"1097"},"2":{"name":"Baskerville","type":"prison","region":"2","phone":"(434) 447-3857","address":{"street":"4150 Hayes Mill Road","city":"Baskerville","zip":"23915"},"coords":{"lat":"36.7224434","long":"-78.3016312"},"Warden":"Earl Barksdale","securityLevel":"multiple","population":"488"},"3":{"name":"Bland","type":"prison","region":"1","phone":"(276) 688-3341","address":{"street":"256 Bland Farm Road","city":"Bland","zip":"24315-9615"},"coords":{"lat":"37.1690048","long":"-80.8854914"},"Warden":"Darrell Miller","securityLevel":"1-2","population":"621"},"4":{"name":"Brunswick Work Center","type":"prison","region":"3","phone":"(434) 848-4131","address":{"street":"1147 Planters Road","city":"Lawrenceville","zip":"23868"},"coords":{"lat":"36.7750866","long":"-77.8190578"},"Warden":"Tammy Brown","securityLevel":"multiple","population":"708"},"5":{"name":"Buckingham","type":"prison","region":"2","phone":"(434) 983-4400","address":{"street":"1349 Correctional Center Road","city":"Dillwyn","zip":"23936"},"coords":{"lat":"37.555754","long":"-78.469707"},"Warden":"Bernard W. Booker","securityLevel":"3-4","population":"1,038"},"6":{"name":"Caroline Correctional Unit #2","type":"prison","region":"3","phone":"(804) 994-2161","address":{"street":"31285 Camp Road","city":"Hanover","zip":"23069"},"coords":{"lat":"37.8328692","long":"-77.3322258"},"Warden":"Bernard W. Booker","securityLevel":"N/A","population":"137"},"7":{"name":"Central Virginia Correctional Unit #13","type":"prison","region":"2","phone":"(804)796-4277","address":{"street":"6900 Courthouse Road","city":"Chesterfield","zip":"23832"},"coords":{"lat":"37.4034826","long":"-77.563072"},"Warden":"Tammy Estep","securityLevel":"N/A","population":"250"},"8":{"name":"Coffeewood","type":"prison","region":"2","phone":"(540) 829-6483","address":{"street":"12352 Coffeewood Drive","city":"Mitchells","zip":"22729"},"coords":{"lat":"38.3610505","long":"-78.0185483"},"Warden":"Ivan Gilmore, Warden","securityLevel":"2","population":"1,193"},"10":{"name":"Cold Springs Correctional Unit #10","type":"prison","region":"1","phone":"(540) 337-1818","address":{"street":"221 Spitler Circle","city":"Greenville","zip":"24440"},"coords":{"lat":"37.991785","long":"-79.154135"},"Warden":"Thomas Redman","securityLevel":"N/A","population":"150"},"11":{"name":"Deep Meadow","type":"prison","region":"2","phone":"(804) 598-5503","address":{"street":"3500 Woods Way","city":"State Farm","zip":"23160"},"coords":{"lat":"37.6166919","long":"-77.8435369"},"Warden":"John Walrath","securityLevel":"2","population":"840"},"12":{"name":"Deerfield","type":"prison","region":"3","phone":"(434) 658-4368","address":{"street":"21360 Deerfield Drive","city":"Capron","zip":"23829"},"coords":{"lat":"36.7280391","long":"-77.2457323"},"Warden":"Tammy Brown","securityLevel":"2","population":"1,069"},"13":{"name":"Dillwyn","type":"prison","region":"2","phone":"(434) 983-4200","address":{"street":"1522 Prison Road","city":"Dillwyn","zip":"23936"},"coords":{"lat":"37.561164","long":"-78.478331"},"Warden":"Larry T. Edmonds","securityLevel":"2","population":"1,106"},"14":{"name":"Fluvanna Correctional Center for Women","type":"prison","region":"2","phone":"(434) 984-3700","address":{"street":"144 Prison Lane","city":"Troy","zip":"22974"},"coords":{"lat":"37.9847899","long":"-78.2701369"},"Warden":"Jeffrey Dillman","securityLevel":"3","population":"1,199"},"15":{"name":"Green Rock","type":"prison","region":"1","phone":"(434) 797-2000","address":{"street":"475 Green Rock Lane","city":"Chatham","zip":"24531"},"coords":{"lat":"36.80144","long":"-79.421314"},"Warden":"Melvin Davis","securityLevel":"2-3","population":"987"},"16":{"name":"Greensville Work Center","type":"prison","region":"3","phone":"(434) 535-7000","address":{"street":"901 Corrections Way","city":"Jarratt","zip":"23870-9614"},"coords":{"lat":"36.7960549","long":"-77.477597"},"Warden":"Eddie L. Pearson","securityLevel":"2-3","population":"3,055"},"17":{"name":"Halifax Correctional Unit #23","type":"prison","region":"2","phone":"(434) 572-2683","address":{"street":"1200 Farm Road","city":"Halifax","zip":"24558"},"coords":{"lat":"36.7278066","long":"-78.9300425"},"Warden":"J.R. Townsend","securityLevel":"N/A","population":"248"},"18":{"name":"Haynesville","type":"prison","region":"3","phone":"(804) 333-3577","address":{"street":"421 Barnfield Road","city":"Haynesville","zip":"22472"},"coords":{"lat":"37.957518","long":"-76.670612"},"Warden":"Israel Hamilton","securityLevel":"2","population":"1,141"},"19":{"name":"Haynesville Correctional Unit #17","type":"prison","region":"3","phone":"(804) 333-3577","address":{"street":"Camp Seventeen Road","city":"warsaw","zip":"22472"},"coords":{"lat":"37.955729","long":"-76.6727606"},"Warden":"Patrick Gurney","securityLevel":"N/A","population":"112"},"20":{"name":"Indian creek","type":"prison","region":"3","phone":"(757) 421-0095","address":{"street":"801 Sanderson Road","city":"Chesapeake","zip":"23328-6481"},"coords":{"lat":"36.6168443","long":"-76.1805461"},"Warden":"James S. Keeling","securityLevel":"2","population":"1,002"},"21":{"name":"James River Work Center","type":"prison","region":"2","phone":"(804) 556-7060","address":{"street":"1954 State Farm Road","city":"State Farm","zip":"23160"},"coords":{"lat":"37.6453305","long":"-77.8308186"},"Warden":"John Walrath","securityLevel":"N/A","population":"N/A"},"22":{"name":"Keen Mountain","type":"prison","region":"1","phone":"(276) 498-7411","address":{"street":"State Route 629","city":"Oakwood","zip":"24631"},"coords":{"lat":"37.2273644","long":"-81.9405353"},"Warden":"Clint Davis","securityLevel":"2/3-4","population":"879"},"23":{"name":"Lawrenceville","type":"prison","region":"3","phone":"(434) 848-9349","address":{"street":"1607 Planters Road","city":"Lawrenceville","zip":"23868"},"coords":{"lat":"36.7800402","long":"-77.8139696"},"Warden":"Ed Wright","securityLevel":"3","population":"1,548"},"24":{"name":"Lunenburg","type":"prison","region":"2","phone":"(434) 696-2045","address":{"street":"690 Falls Road","city":"Victoria","zip":"23974"},"coords":{"lat":"37.0164978","long":"-78.2104385"},"Warden":"Dana Ratliffe-Walker","securityLevel":"2","population":"1,186"},"25":{"name":"Marion","type":"prison","region":"1","phone":"(276) 783-7154","address":{"street":"110 Wright Street","city":"Marion","zip":"24354-3145"},"coords":{"lat":"36.8303484","long":"-81.5078994"},"Warden":"Dara Robichaux","securityLevel":"N/A","population":"371"},"26":{"name":"Medical College of Virginia - MCV","type":"secure medical facility","region":"2","phone":"(804) 828-0804","address":{"street":"401 N. 12th Street","city":"Richmond","zip":""},"coords":{"lat":"37.5407383","long":"-77.4295872"},"Warden":"N/A","securityLevel":"N/A","population":"N/A"},"27":{"name":"Nottoway","type":"prison","region":"2","phone":"(434) 767-5543","address":{"street":"2892 Schutt Road","city":"Burkeville","zip":"23922"},"coords":{"lat":"37.1816808","long":"-78.1759321"},"Warden":"Carl Manis","securityLevel":"3","population":"1,112"},"28":{"name":"Patrick Henry Correctional Unit #28","type":"prison","region":"1","phone":"(276) 957-2234","address":{"street":"18155 A. L. Philpott Highway","city":"Ridgeway","zip":"24148"},"coords":{"lat":"36.6266254","long":"-79.9544897"},"Warden":"Garette Williams","securityLevel":"N/A","population":"136"},"29":{"name":"Pocahontas State","type":"prison","region":"1","phone":"(276) 945-9173","address":{"street":"317 Old Mountain Road","city":"Pocahontas","zip":"24635-0518"},"coords":{"lat":"37.3019956","long":"-81.3596553"},"Warden":"Stan Young","securityLevel":"N/A","population":"910"},"30":{"name":"Powhatan Reception Center","type":"prison","region":"2","phone":"(804) 598-4251","address":{"street":"3600 Woods Way","city":"State Farm","zip":"23160"},"coords":{"lat":"37.6210995","long":"-77.8445578"},"Warden":"John Walrath","securityLevel":"N/A","population":"449"},"31":{"name":"Red Onion","type":"prison","region":"1","phone":"(276) 796-7510","address":{"street":"10800 H. Jack Rose Highway","city":"Pound","zip":"24279"},"coords":{"lat":"37.1112169","long":"-82.5485095"},"Warden":"Jeffrey Kiser","securityLevel":"5-6","population":"799"},"32":{"name":"River North","type":"prison","region":"1","phone":"(276) 773-2518","address":{"street":"329 Dell Brook Lane","city":"Independence","zip":"24348"},"coords":{"lat":"36.6198353","long":"-81.1292143"},"Warden":"Barry Kanode","securityLevel":"4","population":""},"33":{"name":"Rustburg Correctional Unit #9","type":"prison","region":"2","phone":"(434) 332-7354","address":{"street":"479 Camp Nine Road","city":"Rustburg","zip":"24588"},"coords":{"lat":"37.2676449","long":"-79.0705896"},"Warden":"Clint D. Davis","securityLevel":"N/A","population":"152"},"34":{"name":"St. Brides","type":"prison","region":"3","phone":"(757) 421-6600","address":{"street":"701 Sanderson Road","city":" Chesapeake","zip":"23328-6482"},"coords":{"lat":"36.6115529","long":"-76.1836685"},"Warden":"James Beale","securityLevel":"2","population":""},"35":{"name":"Sussex I","type":"prison","region":"3","phone":"(804) 834-9967","address":{"street":"24414 Musselwhite Drive","city":" Waverly","zip":"23891-1111"},"coords":{"lat":"37.0469986","long":"-77.2044357"},"Warden":"David Zook","securityLevel":"4.5","population":"1,139"},"36":{"name":"Sussex II","type":"prison","region":"3","phone":"(804) 834-2678","address":{"street":"24427 Musselwhite Drive","city":" Waverly","zip":"23891-2222"},"coords":{"lat":"37.0517946","long":"-77.2047029"},"Warden":"Tracy Ray","securityLevel":"4","population":"1,269"},"37":{"name":"Virginia Correctional Center for Women","type":"prison","region":"2","phone":"(804) 556-7500","address":{"street":"2841 River Road","city":" Goochland","zip":"23063"},"coords":{"lat":"37.6740987","long":"-77.8900696"},"Warden":"Eric Aldridge","securityLevel":"2","population":"572"},"38":{"name":"Wallens Ridge","type":"prison","region":"1","phone":"(276) 523-3310","address":{"street":"272 Dogwood Drive","city":"Big Stone Gap","zip":"24219"},"coords":{"lat":"36.8499369","long":"-82.7776927"},"Warden":"Leslie Fleming","securityLevel":"5","population":"1200"},"39":{"name":"Wise","type":"prison","region":"1","phone":"(276) 679-9204","address":{"street":"3602 Bear Lane","city":"Coeburn","zip":"24230"},"coords":{"lat":"36.9165044","long":"-82.4622541"},"Warden":"Lafayette Fleming","securityLevel":"N/A","population":"108"},"40":{"name":"Western Regional Office","type":"probation and parole","region":"1","phone":"(540) 561-7050","address":{"street":"5427 Peters Creek Road","city":"Roanoke","zip":"24019"},"coords":{"lat":"37.3360635","long":"-79.9722112"},"Warden":"Henry J. Ponton Jr.","securityLevel":"N/A","population":"N/A"},"41":{"name":"Probation \u0026 Parole District 12","type":"probation and parole","region":"1","phone":"(540) 332-7780","address":{"street":"500 Commerce Road","city":"Staunton","zip":"24402"},"coords":{"lat":"38.15422","long":"-79.057745"},"Warden":"","securityLevel":"N/A","population":"N/A"},"42":{"name":"Probation \u0026 Parole District 13","type":"probation and parole","region":"1","phone":"(434) 947-6651","address":{"street":"725 Church Street","city":"Lynchburg","zip":"24504"},"coords":{"lat":"37.415116","long":"-79.144288"},"Warden":"","securityLevel":"N/A","population":"N/A"},"43":{"name":"Probation \u0026 Parole District 14","type":"probation and parole","region":"1","phone":"(434) 791-5231","address":{"street":"220 Deer Run Road","city":"Danville","zip":"24540"},"coords":{"lat":"36.6140692","long":"-79.410413"},"Warden":"","securityLevel":"N/A","population":"N/A"},"44":{"name":"Probation \u0026 Parole District 15","type":"probation and parole","region":"1","phone":"(540) 387-5257","address":{"street":"305 Electric Road","city":"Salem","zip":"24153"},"coords":{"lat":"37.2925001","long":"-80.028364"},"Warden":"","securityLevel":"N/A","population":"N/A"},"45":{"name":"Probation \u0026 Parole District 16","type":"probation and parole","region":"1","phone":"(276) 228-5311","address":{"street":"335 West Monroe St","city":"Wytheville","zip":"24382"},"coords":{"lat":"36.9519764","long":"-81.0821293"},"Warden":"","securityLevel":"N/A","population":"N/A"},"46":{"name":"Probation \u0026 Parole District 17","type":"probation and parole","region":"1","phone":"(276) 228-5311","address":{"street":"252 West Main Street","city":"Abingdon","zip":"24210"},"coords":{"lat":"36.7089794","long":"-81.9789129"},"Warden":"","securityLevel":"N/A","population":"N/A"},"47":{"name":"Probation \u0026 Parole District 18","type":"probation and parole","region":"1","phone":"(276) 679-9201","address":{"street":"1650 Park Avenue, SW","city":"Norton","zip":"24273"},"coords":{"lat":"36.9306686","long":"-82.6451551"},"Warden":"","securityLevel":"N/A","population":"N/A"},"48":{"name":"Probation \u0026 Parole District 20","type":"probation and parole","region":"1","phone":"(540) 586-7920","address":{"street":"107-C Turnpike Road","city":"Bedford","zip":"24523"},"coords":{"lat":"37.325881","long":"-79.253828"},"Warden":"","securityLevel":"N/A","population":"N/A"},"49":{"name":"Probation \u0026 Parole District 22","type":"probation and parole","region":"1","phone":"(540) 586-7920","address":{"street":"32 Bridge Street","city":"Martinsville","zip":"24112"},"coords":{"lat":"36.6895524","long":"-79.874264"},"Warden":"","securityLevel":"N/A","population":"N/A"},"50":{"name":"Probation \u0026 Parole District 28","type":"probation and parole","region":"1","phone":"(540) 831-5850","address":{"street":"2003 West Main Street","city":"Radford","zip":"24141"},"coords":{"lat":"37.1156113","long":"-80.5903735"},"Warden":"","securityLevel":"N/A","population":"N/A"},"51":{"name":"Probation \u0026 Parole District 37","type":"probation and parole","region":"1","phone":"(540) 483-0854","address":{"street":"245 Circle Drive","city":"Rocky Mount","zip":"24151"},"coords":{"lat":"37.0087126","long":"-79.8944989"},"Warden":"","securityLevel":"N/A","population":"N/A"},"52":{"name":"Probation \u0026 Parole District 39","type":"probation and parole","region":"1","phone":"(540) 433-2404","address":{"street":"30-A West Water Street","city":"Harrisonburg","zip":"22801"},"coords":{"lat":"38.4486498","long":"-78.8696815"},"Warden":"","securityLevel":"N/A","population":"N/A"},"53":{"name":"Probation \u0026 Parole District 40","type":"probation and parole","region":"1","phone":"(540) 473-2056","address":{"street":"450 South Church Street","city":"Fincastle","zip":"24090"},"coords":{"lat":"37.4923049","long":"-79.8765874"},"Warden":"","securityLevel":"N/A","population":"N/A"},"54":{"name":"Probation \u0026 Parole District 1","type":"probation and parole","region":"2","phone":"(804) 786-0251","address":{"street":"829 Oliver Hill Way","city":"Richmond","zip":"23219"},"coords":{"lat":"37.5407938","long":"-77.4241661"},"Warden":"","securityLevel":"N/A","population":"N/A"},"55":{"name":"Probation \u0026 Parole District 8","type":"probation and parole","region":"2","phone":"(434) 575-5774","address":{"street":"2510 Houghton Avenue","city":"South Boston","zip":"24592"},"coords":{"lat":"36.7209804","long":"-78.8925461"},"Warden":"","securityLevel":"N/A","population":"N/A"},"56":{"name":"Probation \u0026 Parole District 9","type":"probation and parole","region":"2","phone":"(434) 295-7194","address":{"street":"750 Harris Street","city":"Charlottesville","zip":"22903"},"coords":{"lat":"38.0363326","long":"-78.4834687"},"Warden":"","securityLevel":"N/A","population":"N/A"},"57":{"name":"Probation \u0026 Parole District 10","type":"probation and parole","region":"2","phone":"(703) 875-0100","address":{"street":"3300 North Fairfax Drive","city":"Arlington","zip":"22201"},"coords":{"lat":"38.8843699","long":"-77.0998106"},"Warden":"","securityLevel":"N/A","population":"N/A"},"58":{"name":"Probation \u0026 Parole District 11","type":"probation and parole","region":"2","phone":"(540) 722-3404","address":{"street":"100 Premier Place","city":"Winchester","zip":"22602"},"coords":{"lat":"39.1512902","long":"-78.1611796"},"Warden":"","securityLevel":"N/A","population":"N/A"},"59":{"name":"Probation \u0026 Parole District 21","type":"probation and parole","region":"2","phone":"(540) 710-2102","address":{"street":"5620 Southpoint Centre Blvd.","city":"Fredricksburg","zip":"22407-2601"},"coords":{"lat":"38.2287186","long":"-77.5018591"},"Warden":"","securityLevel":"N/A","population":"N/A"},"60":{"name":"Probation \u0026 Parole District 24","type":"probation and parole","region":"2","phone":"(434) 392-8671","address":{"street":"601 Industrial Park Road","city":"Farmville","zip":"23901"},"coords":{"lat":"37.3201022","long":"-78.4244512"},"Warden":"","securityLevel":"N/A","population":"N/A"},"61":{"name":"Probation \u0026 Parole District 25","type":"probation and parole","region":"2","phone":"(703) 771-2510","address":{"street":"751-D Miller Drive","city":"Leesburg","zip":"20175"},"coords":{"lat":"39.0825288","long":"-77.5564265"},"Warden":"","securityLevel":"N/A","population":"N/A"},"62":{"name":"Probation \u0026 Parole District 2","type":"probation and parole","region":"3","phone":"(757) 683-8417","address":{"street":"861 Monticello Avenue","city":"Norfolk","zip":"23510"},"coords":{"lat":"36.8586099","long":"-76.2874699"},"Warden":"","securityLevel":"N/A","population":"N/A"},"63":{"name":"Probation \u0026 Parole District 3","type":"probation and parole","region":"3","phone":"(757) 396-6845","address":{"street":"601 PortCentre Parkway","city":"Portsmouth","zip":"23704"},"coords":{"lat":"36.8261112","long":"-76.2980483"},"Warden":"","securityLevel":"N/A","population":"N/A"},"64":{"name":"Probation \u0026 Parole District 4","type":"probation and parole","region":"3","phone":"(757) 396-6845","address":{"street":"23328 Front Street","city":"Accomac","zip":"23301"},"coords":{"lat":"37.7187421","long":"-75.6714111"},"Warden":"","securityLevel":"N/A","population":"N/A"},"65":{"name":"Probation \u0026 Parole District 5","type":"probation and parole","region":"3","phone":"(757) 396-6845","address":{"street":"6270 Professional Drive","city":"Gloucester","zip":"23061"},"coords":{"lat":"37.385286","long":"-76.530479"},"Warden":"","securityLevel":"N/A","population":"N/A"},"66":{"name":"Probation \u0026 Parole District 6","type":"probation and parole","region":"3","phone":"(757) 925-2278","address":{"street":"425 West Washington Street","city":"Suffolk","zip":"23804"},"coords":{"lat":"36.7292392","long":"-76.5882465"},"Warden":"","securityLevel":"N/A","population":"N/A"},"67":{"name":"Probation \u0026 Parole District 7","type":"probation and parole","region":"3","phone":"(804) 524-6542","address":{"street":"26317 W. Washington St.","city":"Petersburg","zip":"23804"},"coords":{"lat":"37.2159518","long":"-77.4501302"},"Warden":"","securityLevel":"N/A","population":"N/A"},"68":{"name":"Probation \u0026 Parole District 19","type":"probation and parole","region":"3","phone":"(757) 247-8000","address":{"street":"2506 Warwick Blvd.","city":"Newport News","zip":"23607"},"coords":{"lat":"36.9796421","long":"-76.4262078"},"Warden":"","securityLevel":"N/A","population":"N/A"},"69":{"name":"Probation \u0026 Parole District 23","type":"probation and parole","region":"3","phone":"(757) 821-7575","address":{"street":"2520 Nimmo Parkway","city":"Virginia Beach","zip":"23456-9125"},"coords":{"lat":"36.753599","long":"-76.066062"},"Warden":"","securityLevel":"N/A","population":"N/A"},"70":{"name":"Eastern Regional Office","type":"probation and parole","region":"3","phone":"(434) 658-4368","address":{"street":"14545 Old Belfield Road","city":"Capron","zip":"23829"},"coords":{"lat":"36.7223511","long":"-77.2553662"},"Warden":"Jamilla Burney-Divens","securityLevel":"N/A","population":"N/A"},"71":{"name":"Southampton Men's Detention Center","type":"community facility","region":"3","phone":"(434) 658-4174","address":{"street":"14380 Terrapin Drive","city":"Capron","zip":"23829"},"coords":{"lat":"36.7165707","long":"-77.2580248"},"Warden":"","securityLevel":"N/A","population":"N/A"},"72":{"name":"Chesterfield Women's Detention Center","type":"community facility","region":"2","phone":"(434) 658-4174","address":{"street":"7000 Courthouse Road","city":"Chesterfield","zip":"23832"},"coords":{"lat":"37.4021757","long":"-77.5585843"},"Warden":"","securityLevel":"N/A","population":"N/A"},"73":{"name":"Appalachian Men's Detention Center","type":"community facility","region":"1","phone":"(276) 889-7671","address":{"street":"924 Clifton Farm Road","city":"Honaker","zip":"24260"},"coords":{"lat":"36.9987478","long":"-81.9313692"},"Warden":"","securityLevel":"N/A","population":"N/A"},"74":{"name":"Stafford Men's Diversion Center","type":"community facility","region":"2","phone":"(540) 659-2093","address":{"street":"384 Eskimo Hill Road","city":"Stafford","zip":"22554"},"coords":{"lat":"38.3802813","long":"-77.4210193"},"Warden":"","securityLevel":"N/A","population":"N/A"},"75":{"name":"Harrisonburg Men's Diversion Center","type":"community facility","region":"2","phone":"(540) 833-2011","address":{"street":"6624 Beard Woods Lane","city":"Harrisonburg","zip":"22802"},"coords":{"lat":"38.5232813","long":"-78.830243"},"Warden":"","securityLevel":"N/A","population":"N/A"},"76":{"name":"Atmore Headquarters","type":"community facility","region":"2","phone":"(804) 833-2011","address":{"street":"6900 Atmore Dr.","city":"Richmond","zip":"23225"},"coords":{"lat":"37.504648","long":"-77.525150"},"Warden":"","securityLevel":"N/A","population":"N/A"}}},
        facilitiesSort: $('#results-sort') 
    }
    ////////////////////////////
    //-----Facilities Map-------
    ////////////////////////////
    //Map Styles
    var styledMap = new google.maps.StyledMapType(
        [
    {
        "featureType": "water",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#e9e9e9"
            },
            {
                "lightness": 17
            }
        ]
    },
    {
        "featureType": "landscape",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 0
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 100
            }
        ]
    },
    {
        "featureType": "road.highway",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 29
            },
            {
                "weight": 1.0
            }
        ]
    },
    {
        "featureType": "road.arterial",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 18
            }
        ]
    },
    {
        "featureType": "road.local",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "featureType": "poi",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f5f5f5"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "featureType": "poi.park",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#dedede"
            },
            {
                "lightness": 21
            }
        ]
    },
    {
        "elementType": "labels.text.stroke",
        "stylers": [
            {
                "visibility": "on"
            },
            {
                "color": "#ffffff"
            },
            {
                "lightness": 16
            }
        ]
    },
    {
        "elementType": "labels.text.fill",
        "stylers": [
            {
                "saturation": 36
            },
            {
                "color": "#333333"
            },
            {
                "lightness": 40
            }
        ]
    },
    {
        "elementType": "labels.icon",
        "stylers": [
            {
                "visibility": "off"
            }
        ]
    },
    {
        "featureType": "transit",
        "elementType": "geometry",
        "stylers": [
            {
                "color": "#f2f2f2"
            },
            {
                "lightness": 19
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.fill",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 20
            }
        ]
    },
    {
        "featureType": "administrative",
        "elementType": "geometry.stroke",
        "stylers": [
            {
                "color": "#fefefe"
            },
            {
                "lightness": 17
            },
            {
                "weight": 1.2
            }
        ]
    }
]);
    //Parameters for facilities map
    var mapOptions = {
      disableDefaultUI: true,
      backgroundColor: '#FFF',
      draggable: true,
      panControl: true,
      mapTypeControl: true,
      mapTypeControlOptions: {
          mapTypeIds: [google.maps.MapTypeId.ROADMAP, google.maps.MapTypeId.HYBRID, google.maps.MapTypeId.styledMapType],
          style: google.maps.MapTypeControlStyle.DROPDOWN_MENU
      },
      scaleControl: true,
      scrollwheel: false,
      zoomControl: false,
      streetViewControl: false,
      rotateControl: true,
      overviewMapControl: true,
      overviewMapControlOptions: {
          opened: true
      }
    };
    
    //Get list of facilities with content/data and populate page
    function populateData(map) {
      if(defaults.facility) {
        var facility = defaults.facility;
        drawFacilityMarker(map, facility.coords.lat, facility.coords.long, 
                                {name: facility.name,
                                  phone: facility.phone || '(123) 456-7890',
                                  type: facility.type, 
                                  link: '#', 
                                  address: {
                                    street: facility.address.street, 
                                    city: facility.address.city, 
                                    poBox: facility.address.poBox || 'P.O. Box 999', 
                                    state: 'VA', 
                                    zip: facility.address.zip 
                                  }
                                } 
        );
      } else {
        var data = defaults.data;
        
        $.each(data.facilities, function(facilityNumber, facility) { 
          drawFacilityMarker(map, facility.coords.lat, facility.coords.long, 
                            {name: facility.name, 
                              id: facilityNumber, 
                              phone: facility.phone || '(123) 456-7890',
                              type: facility.type, 
                              link: '#', 
                              address: {
                                street: facility.address.street, 
                                city: facility.address.city, 
                                poBox: facility.address.poBox || 'P.O. Box 999', 
                                state: 'VA', 
                                zip: facility.address.zip 
                              }
                            } 
          );
        })
      }
    }
    ///////////////////////////
    //---Facility Map Markers
    ///////////////////////////
    function drawFacilityMarker(map, lat, lng, args) {
        var latLng = new google.maps.LatLng(lat, lng);
        new CustomMarker(
            latLng,
            map,
            {
                id: args.id,
                title: args.name,
                type: args.type,
                link: args.link,
                phone: args.phone,
                address:
                {
                    street: args.address.street,
                    city: args.address.city,
                    state: 'VA',
                    zip: args.address.zip,
                    poBox: args.address.poBox,
                    lat: lat,
                    lng: lng
                },
            }
        )
    }

    var drawGoogleMarker = function (map, LatLngObj, args) {
        return new google.maps.Marker({
            position: LatLngObj,
            map: map,
            title: args.title,
        });
    }

    function CustomMarker(latlng, map, args) {
        this.latlng = latlng;
        this.args = args;
        this.setMap(map);
    }
    CustomMarker.prototype = new google.maps.OverlayView();
    CustomMarker.prototype.draw = function () {
        var self = this;
        var div = this.div;
        var googleMarker = drawGoogleMarker(self.map, self.latlng, { title: self.args.title });
        googleMarker.setVisible(false);
        var infoWindow = new google.maps.InfoWindow();

        if (!div) {
            div = this.div = document.createElement('div');

            if (self.args.type === "prison") {
                div.className = 'facility-marker prison';
            } else if (self.args.type === "community facility") {
                div.className = 'facility-marker community';
            } else if (self.args.type === "probation and parole") {
                div.className = 'facility-marker probation-parole';
            } else if (self.args.type === "secure medical facility") {
                div.className = 'facility-marker secure-medical';
            } else if (self.args.type === "a starbucks") {
                div.className = 'facility-marker regional';
            }

            if (this.args.id) {
                $(div).attr('data-id', this.args.id || "");
            }
            if (typeof (self.args.marker_id) != 'undefined') {
                div.dataset.marker_id = self.args.marker_id;
            }
            google.maps.event.addDomListener(div, 'click', function () {
                var name = self.args.title
                var address = self.args.address;
                var street = address.street;
                var state = address.state;
                var city = address.city;
                var zip = address.zip;
                var poBox = address.poBox;
                var phone = self.args.phone;
                var type = self.args.type
                var lat = address.lat;
                var long = address.lng;
                var link = self.args.link;
                var contentString = '<div class="marker-info-box" id="' + self.args.id + '">' +
										'<p class="facility-page-link"><a class="body-bold" href="#">' + name + '</a></p>' +
										'<p>' + street + '</p>' +
                                        '<p>' + poBox + '</p>' +
										'<p class="city-state-zip">' + city + ',&nbsp;' + state + '&nbsp;' + zip + '</p>' +
										'<p class="info-box-phone">' + phone + '</p>' +
										'<a href="#" onclick="showDirectionsSearch(' + lat + ',' + long + ')" id="' + self.args.id + '-directions">' +
											'Directions' + 
										'</a>' +
									'</div>'
                infoWindow.setContent(contentString);
                infoWindow.open(self.map, googleMarker);
                return false;
            });

            var panes = this.getPanes();
            panes.overlayImage.appendChild(div);
        }

        var point = this.getProjection().fromLatLngToDivPixel(this.latlng);

        if (point) {
            div.style.left = (point.x - 10) + 'px';
            div.style.top = (point.y - 20) + 'px';
        }
    }
    //////////////////
    //---Initialize
    //////////////////
    var facilityId = parseInt($('main')[0].id);
    function initialize(args) {
        if(!args) {
            args = {};
        }
        if (facilityId > 0) {
            defaults.facility = defaults.data.facilities[facilityId];
            var lat = parseFloat(defaults.facility.coords.lat);
            var lng = parseFloat(defaults.facility.coords.long);
            mapOptions.center = new google.maps.LatLng(lat, lng);
            mapOptions.zoom = 11;
        } else {
            mapOptions.center = new google.maps.LatLng(37.9316, -79.6569);
            mapOptions.zoom = 8;
        }
        //Create facilities map
        var facilitiesMap = new google.maps.Map(document.getElementById('facilities-map'), mapOptions);
        facilitiesMap.mapTypes.set('styled_map', styledMap);
        facilitiesMap.setMapTypeId('styled_map');
        //Request google directions service object and directions service renderer
        directionsService = new google.maps.DirectionsService;
        var directionsPanel = document.getElementById('directions-panel');
        directionsDisplay = new google.maps.DirectionsRenderer({map: facilitiesMap, panel: directionsPanel}); 
        //Add KML State border layer
        if (!(facilityId > 0)) {
        	var facilitiesFilterWrapper = $('#facilities-filter');
        	var facilitiesFilter = $('#facilities-filter ul');
            var vaKmlLayer = new google.maps.KmlLayer({
                url: 'https://www.dropbox.com/s/cymmvjny11lvfdb/virginia.kml?dl=1',
                map: facilitiesMap
            });
            google.maps.event.addDomListener(vaKmlLayer, 'click', function (event) {
                facilitiesMap.setOptions({
                    scrollwheel: true,
                    zoomControl: true
                });
            });
         //    var ctaLayer = new google.maps.KmlLayer({
	        //   url: 'https://www.dropbox.com/s/k5cihs1yy7sg7de/VaCounties.kml?dl=1',
	        //   map: facilitiesMap
	        // });
            $(facilitiesFilter).css({
            	'width': $(facilitiesFilter).width(),
            	'margin-left': ($(facilitiesFilterWrapper).width() - $(facilitiesFilter).width()) * 0.5
            });
        }       
        //Populate facility data
        populateData(facilitiesMap);
        //Add click functionality to facility filter checkboxes
        $.each(filterOptions, function(i, optionCheckbox) {
            $(optionCheckbox).click(function() {
                // optionCheckbox.toggle('checked');
                setFilterOptions();
            })
        })
        // Show facility sort buttons
        defaults.facilitiesSort.show();
    }
/////////////////////////
//---Facilities Filter
////////////////////////
var filterListTrigger = $('#show-filter');
filterListTrigger.on('click', function(e) {
	e.preventDefault();
	e.stopPropagation();

	var list = $('#facilities-filter').find('ul');

	$(this).closest('#facilities-filter-wrap').toggleClass('open');
	$(list).toggle(300);
})
var filterOptions = $('#facilities-filter').find('input[type="checkbox"]');
function setFilterOptions() {
    var currentFilters = [];

    for(var i = 0; i < filterOptions.length; i++) {
        if(!(filterOptions[i].checked)) {
            currentFilters.push(filterOptions[i].id);
        }
    }
    filterFacilities(currentFilters);
}
function filterFacilities(filters) {
    $.each(filterOptions, function(i, checkbox) {
        var filterClass = '.' + checkbox.id;
        if($.inArray(checkbox.id, filters) >= 0) {
            $(filterClass).hide(200);
        } else {
            $(filterClass).show(200);
        }
    });
}
/////////////////////////
//--Directions Service
/////////////////////////
var directionsService;
var directionsDisplay;

function showDirectionsSearch(lat, long) {
    $('#geocoder').show(400).find('#origin-address').attr('data-lat', lat).attr('data-long', long);
}

function getDirections() {
    var directionsApiKey = "AIzaSyBRjesUdoFdUKkkjN27Ntk-sYhjPhkySwc";   
    var directionsApiRequestString = "http://maps.googleapis.com/maps/api/directions/json?"
    var addressInput = $('#origin-address');
    var destinationLat = addressInput.attr('data-lat');
    var destinationLng = addressInput.attr('data-long');
    var destinationLatLng = new google.maps.LatLng(destinationLat, destinationLng);
    var originStrng = addressInput.val();
    directionsService.route({
        origin: originStrng,
        destination: destinationLatLng,
        travelMode: 'DRIVING',
        provideRouteAlternatives: true
    }, function(response, status) {
        if (status == google.maps.DirectionsStatus.OK) {
            directionsDisplay.setDirections(response)
            $('#geocoder').hide(500);
            $('#directions-panel').show(500);
            $('#close-directions').click(function() {
                $('#directions-panel').hide(500);
            })
        } else {
            console.log(status);
        }
    });
}
////////////////////////////////////////////////////
//--Geocoder to produce coordinates from address
///////////////////////////////////////////////////
var geocoder = new google.maps.Geocoder();
function geocodeAddress(address) {
    geocoder.geocode( { 'address': address}, function(results, status) {
      if (status == 'OK') {
        debugger 
      } else {
        alert('Geocode was not successful for the following reason: ' + status);
      }
    });
}