﻿<?xml version="1.0" encoding="utf-8"?>
<pageWithSecondaryNav guid="35455fa7-57cd-4ec4-ae1b-617d2441d9ea" id="1148" nodeName="Notifications" isDoc="" updated="2019-02-14T11:43:49.6034669Z" parentGUID="dae21633-0767-4660-b37a-53c165e8689c" nodeTypeAlias="pageWithSecondaryNav" templateAlias="PageWithSecondaryNav" sortOrder="0" published="true" isBlueprint="false">
  <alertContent></alertContent>
  <alertHeading></alertHeading>
  <contactCardHeading></contactCardHeading>
  <contactDisclaimer></contactDisclaimer>
  <daysOfOperation></daysOfOperation>
  <displaySidebarContactCard>0</displaySidebarContactCard>
  <hoursOfOperation></hoursOfOperation>
  <mainContent><![CDATA[<!-- In-page Contact Link -->
<div>
<a id="in-page-contact" href="#hcard-main-contact" class="primary-button">contact us</a>
</div>
<!-- End In-page Contact Link -->
<div class="description">
<p>One of the ways our team supports victims of crime is by providing timely notification of offender updates.</p>
</div>
<section id="register-for-notifications">
<h2>Register for Notifications</h2>
<div class="description">
<p>Apply to receive notifications on offender status through VINE. Complete the online form to register.</p>
<p>As a victim of crime <a>Code of Virginia §19.2-11.01</a>, you can register to receive notifications on an offenders’ status in two steps:</p>
</div>
<div id="complete-form" class="content-callout"><img class="icon-callout" src="/images/victim-services/complete-form.svg" alt="" aria-hidden="true" />
<div class="icon-callout-item">
<h3>Complete the Form</h3>
<p>Fill out the Victim Notification form.</p>
</div>
<a class="primary-button">fill out the form</a></div>
<div class="content-callout"><img class="icon-callout" src="/images/victim-services/send-form.svg" alt="" aria-hidden="true" />
<div class="icon-callout-item">
<h3>Send the finished form by:</h3>
<p class="body-bold">Mail</p>
<div class="vcard">
<p class="adr"><span class="org">Virginia Department of Corrections</span><br /><span class="attn">Attention: Victim Services</span><br /><span class="post-office-box"> P.O. Box 26963 </span><br /><span class="locality">Richmond</span>, <span class="region"> VA<span></span> <span class="postal-code"> 23225</span> </span></p>
<p class="or">OR</p>
<p class="tel"><span class="body-bold type">Fax</span><br />(804) 123-4567<br /><span class="attn">Attention: Victim Services</span></p>
</div>
</div>
</div>
<p>If you need help with registering, call our office at <a href="tel:1-800-560-4294">1 (800) 560-4294</a><br /><span class="body-bold">This service is completely confidential. Your contact information and registration is never released to inmates under any circumstances.</span></p>
<div id="discontinue-notifications" class="dark-background"><img class="icon-callout" src="/images/victim-services/stop-notifications.svg" alt="" />
<div class="icon-callout-item">
<h2>Discontinue Notifications</h2>
<p>if you would like to stop receiving notifications, please submit your request in writing to our office at:</p>
<p class="body-bold">VADOC–Victim Services</p>
<p class="addr">6900 Atmore Drive, Richmond, VA 23225</p>
</div>
</div>
</section>
<section id="notification-types" class="alternate-background">
<h2>Types of Notifications</h2>
<div class="description">
<p>When you register for notifications, we will send you updates by phone and/or email of an offender's:</p>
</div>
<ul class="unstyled-list">
<li>
<h3>Transfer to another facility</h3>
<p>An offender is moved to another jail or prison.</p>
</li>
<li>
<h3>Release date</h3>
<p>An offender is freed from VADOC custody.</p>
</li>
<li>
<h3>Release date</h3>
<p>An offender is freed from VADOC custody.</p>
</li>
<li>
<h3>Death</h3>
<p>An offender dies.</p>
</li>
<li>
<h3>Name Change</h3>
<p>An offender changes their alias or legal name.</p>
</li>
<li>
<h3>Work Release Status</h3>
<p>An offender is allowed to go to work each day and return to jail when their shift is complete.</p>
</li>
<li>
<h3>Escape</h3>
<p>An offender unlawfully breaks free from VADOC custody.</p>
</li>
<li>
<h3>Recapture</h3>
<p>An offender is arrested and in VADOC custody again.</p>
</li>
<li>
<h3>Parole Interview</h3>
<p>An offender is eligible for parole and is scheduled for a parole interview or hearing.</p>
</li>
<li>
<h3>Parole Decision</h3>
<p>An offender is arrested and in VADOC custody again.</p>
</li>
</ul>
</section>
<section id="other-notifications">
<h2>Other Notification Services</h2>
<div class="description">
<p>Agencies outside of the VADOC also provide notification services for victims of crime. Learn more about their programs and how to sign up:</p>
</div>
<ul class="icon-bullet-list">
<li><img class="li-icon" src="/images/victim-services/vine.svg" alt="" aria-hidden="true" />
<div class="li-info col-17">
<p><a href="#">VINE</a> <span class="dash">–</span> Local Jail Notifications</p>
</div>
</li>
<li><img class="li-icon" src="/images/victim-services/va-seal-blue.svg" alt="" aria-hidden="true" />
<div class="li-info col-17">
<p><a href="#">Attourney General of Virginia</a> <span class="dash">–</span> Victim Notification Program</p>
</div>
</li>
<li><img class="li-icon" src="/images/victim-services/va-parole.svg" alt="" aria-hidden="true" />
<div class="li-info col-17">
<p><a href="#">Virginia Parole Board</a> <span class="dash">–</span> Victim Input Program</p>
</div>
</li>
</ul>
</section>]]></mainContent>
  <menuTitle></menuTitle>
  <metaDescription></metaDescription>
  <pageClass><![CDATA[victim-services]]></pageClass>
  <pageContentType><![CDATA[Victim Services]]></pageContentType>
  <pageId><![CDATA[victim-services-notifications]]></pageId>
  <pageTitle><![CDATA[Notifications]]></pageTitle>
  <primaryNavItem>0</primaryNavItem>
  <primaryPocAddressAgency></primaryPocAddressAgency>
  <primaryPocAddressAttn></primaryPocAddressAttn>
  <primaryPocAddressUnit></primaryPocAddressUnit>
  <primaryPocCity></primaryPocCity>
  <primaryPocEmail></primaryPocEmail>
  <primaryPocFaxNumber></primaryPocFaxNumber>
  <primaryPocMainContactPhoneLabel></primaryPocMainContactPhoneLabel>
  <primaryPocMainContactPhoneNumber></primaryPocMainContactPhoneNumber>
  <primaryPocName></primaryPocName>
  <primaryPocPostOfficeBox></primaryPocPostOfficeBox>
  <primaryPocSecondaryEmail></primaryPocSecondaryEmail>
  <primaryPocSecondContactPhoneLabel></primaryPocSecondContactPhoneLabel>
  <primaryPocSecondContactPhoneNumber></primaryPocSecondContactPhoneNumber>
  <primaryPocStreetAddress></primaryPocStreetAddress>
  <primaryPocTitle></primaryPocTitle>
  <primaryPocUnit></primaryPocUnit>
  <primaryPocZipCode></primaryPocZipCode>
  <secondaryNavItem>0</secondaryNavItem>
  <umbracoNaviHide>0</umbracoNaviHide>
</pageWithSecondaryNav>