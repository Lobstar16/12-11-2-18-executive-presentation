var minusIconPath = '/images/global/icons/minus.svg';
var plusIconPath = '/images/global/icons/plus.svg';
var openDrawer;

function setOpenDrawer(drawer) {
	openDrawer = drawer;
}

$(function() {
	$('.drawer-icon').css({display: 'inline-block'});
	// Closes drawer
	function closeDrawer(drawer) {
		$(drawer).find('ul').hide(function() {
			$(drawer).addClass('closed-drawer');
		});
	}
	// Opens current drawer and calls changeIcon
	function openCurrentDrawer(drawer) {
		$(drawer).removeClass('closed-drawer').find('ul').slideDown(300);
		changeIcon(drawer);
		$(drawer).css('background-color', '');
	}
	// Switches the +/- icon images according to open or closed status
	function changeIcon(drawer) {
		var drawerIcon = $(drawer).find('.drawer-icon');

		if(openDrawer === drawer) {
			$(drawerIcon).css('background-image', 'url(' + minusIconPath + ')');
		} else {
			$(drawerIcon).css('background-image', 'url(' + plusIconPath + ')');
		}
		$(drawerIcon).toggleClass('close-drawer');
	}
	// ---------------------------------------------------
	// Add necessary drawer UI functionality on page load
	// ---------------------------------------------------
	// Close all drawers and hide nested ul content
	$('.drawer').addClass('closed-drawer').find('ul').hide();
	// Add closed drawer hover functionality via js for Chrome - CSS works for all other browsers
	$('.drawer').hover(function() {
		if($(this).hasClass('closed-drawer')) {
			$(this).css({
				'background-color': '#eef2f4',
				'cursor': 'pointer'
			});
		}
	}, function() {
		if($(this).hasClass('closed-drawer')) {
			$(this).css({
				'background-color': '#fff'
			});
		}
	});
	// Adds drawer focus state for keyboard navigation
	$('.closed-drawer .drawer-icon').focus(function(e) {
		if(this != openDrawer) {
			$(this).closest('.closed-drawer').css('background-color', '#eef2f4');
		}
	}).blur(function(e) {
		$(this).closest('.closed-drawer').css('background-color', 'transparent');
	});;
	// Delegate click handler to all drawers
	$('.closed-drawer').click(function(event) {
		var oldDrawer = openDrawer;
		var newDrawer = $(this).closest('.drawer')[0];
		if(openDrawer != null) {
			if(openDrawer === newDrawer) {
				event.stopPropagation();
			} else {
				closeDrawer(oldDrawer);
				setOpenDrawer(newDrawer);
				changeIcon(oldDrawer);
				openCurrentDrawer(newDrawer);
			}
		} else {
			setOpenDrawer(newDrawer);
			openCurrentDrawer(newDrawer);
		}
	});
	// Set click handler for drawer icon 
	$('.drawer-icon').click(function(event) {
		if($(this).hasClass('close-drawer')) {
			event.stopPropagation();
			var drawer = $(this).closest('.drawer')[0];
			setOpenDrawer(null);
			changeIcon(drawer);
			closeDrawer(drawer);
		}
	});
});