﻿using System;
using System.Web.Http;
using Umbraco.Web;
using vadoc_public_website_redesign.App_Start;

namespace vadoc_public_website_redesign.EventHandlers
{
    public class WebApplication : UmbracoApplication
    {
        protected override void OnApplicationStarting(object sender, EventArgs e)
        {
            base.OnApplicationStarting(sender, e);
            GlobalConfiguration.Configure(WebApiConfig.Register);
        }
    }
}