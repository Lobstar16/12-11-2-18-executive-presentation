﻿var filterSets;
var regionFilters;
var typeFilters;
var stateRegions;
var uiPrompt;
var firstClick;

function checkRegionDisplay() {
    var displayUiPrompt = true;

    $.each(stateRegions, function (i, region) {
        if ($(region).is(":visible")) {
            displayUiPrompt = false;
        }
    });

    displayUiPrompt ? uiPrompt.fadeIn(300) : uiPrompt.hide();
}

$(function () {
    firstClick = true;
    filterSets = $(".button-group");
    regionFilters = $("#region-filter button");
    typeFilters = $("#type-filter button");
    stateRegions = $(".state-region");
    uiPrompt = $("#ui-prompt");

    //Hide all regions and facilities
    stateRegions.hide();
    // Show filter sets
    filterSets.show();

    // Filter by region
    regionFilters.on('click', function (event) {
        if (firstClick) {
            typeFilters.addClass("active");
        }

        firstClick = false;
        $(event.target).toggleClass("active");
        var region = $("#" + event.target.id + "-region");
        region.toggle(300).promise().done(checkRegionDisplay);
    });

    // Filter by type
    typeFilters.on("click", function (event) {
        var targetId = event.target.id;
        var classList = [];
        $(this).toggleClass("active");

        $.each(typeFilters, function (i, filter) {
            var id = filter.id;
            if ($(filter).hasClass("active")) {
                classList.push(id);
            }
        });

        $.each(stateRegions, function (i, region) {
            $(region).find("ul").hide();
            $(region).find("h3").hide();

            for (var i = 0; i < classList.length; i++) {
                $(region).find("." + classList[i]).fadeIn(200);
            }
        });
    });
});