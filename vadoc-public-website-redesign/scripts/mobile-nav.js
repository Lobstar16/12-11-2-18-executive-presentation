// Globals
var navTrigger;
var searchTrigger;
var main;
var searchForm;

$(function() {
	// Set globals after page loads
	main = $('main');
	navTrigger = $('#open-nav');
	navTriggerTop = $(navTrigger).find('#top');
	navTriggerMiddle = $(navTrigger).find('#middle');
	navTriggerBottom = $(navTrigger).find('#bottom');
	searchTrigger = $('#open-search');
	searchForm = $('#mobile-nav').find('.site-search');
	var modalOverlay = $('#mobile-nav .modal-overlay');
	var modalWindow = $('#mobile-nav .mobile-nav-window');
	var modalContent = $('#mobile-nav .mobile-nav-content');
	var contentWrapper = $(modalWindow).find('.content-wrapper');
	var modalClose = $('.close-mobile-nav');
	var checkBoxInputs = $('.mobile-nav-content input[type=checkbox]');
	// Display Mobile Nav
	function displayMobileNav() {
		$('body').addClass('mobile-nav-open');
		modalOverlay.fadeIn(200);
		modalWindow.show(200);
		modalWindow.animate({
			left: '0'
		}, 500)
	}
	// Close Mobile Nav
	function closeMobileNav() {
		modalWindow.animate({
			left: '-1000'
		}, 500, function() {
			modalWindow.hide(200);	
			modalOverlay.fadeOut(200);
			$('body').removeClass('mobile-nav-open');
			$.each(checkBoxInputs, function(index, input) {
				$(input).prop("checked", false);
			});
		});
	}
	// Display search form
	function displaySearch() {
		searchTrigger.fadeOut('fast');
		searchForm.show();
		searchForm.animate({
			right: '2rem'
		}, 100)
	}
	// Bind click handlers to nav triggers
	searchTrigger.on('click', displaySearch);
	navTrigger.on('click', displayMobileNav);
	modalClose.on('click', closeMobileNav);
});